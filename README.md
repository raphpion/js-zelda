# JS-Zelda

Hello world!
JS-Zelda is a **2D Topdown RPG engine** based on the SNES classic _the Legend of Zelda: A Link to the Past_. It is made with Vanilla Javascript as well as the new canvas functionalities released with HTML5.

Unfortunately, as I have acquired more advanced skills in Javascript, most of the game's functions are now outdated and the whole project would need a revision, so the development has stopped.

## Credits

Thanks to ­[Gabriel Cholette-Rioux](https://gcholette.com/) for his precious help!

Thanks to the amazing [Spriters Resource](https://www.spriters-resource.com/) community for the spritesheets & tilesheets:

- [Mister Man](https://www.spriters-resource.com/submitter/Mister+Man/): Link's sprites
- [Dasani](https://www.spriters-resource.com/submitter/Dasani/): Overworld, House Interior and Dungeon Tiles
- [Davias](https://www.spriters-resource.com/submitter/Davias/): Overworld effects
- [Deathbringer](https://www.spriters-resource.com/submitter/Deathbringer/): Loot sprites
- [Nemau](https://www.spriters-resource.com/submitter/Nemau/): Light World full map

Thanks to [ZharK](https://www.sounds-resource.com/submitter/ZharK/) from the [Sounds Resource](https://www.sounds-resource.com/submitter/ZharK/) for sharing the official ALTTP sounds effects

Thanks to [Zelda Universe](https://zeldauniverse.net/) for sharing the official ALTTP soundtrack
