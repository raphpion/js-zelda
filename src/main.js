//* JS-Zelda Pre-alpha
//? a 2D Top-Down Zelda game engine made with Vanilla JS
// Developped by Raphaël Pion
// All assets owned by Nintendo

import { loadAssets } from './loader.js'
import { resizeScreen } from './screen.js'

window.onresize = resizeScreen

console.log('Lauching game JS-Zelda version Pre-Alpha')
resizeScreen()
loadAssets()
