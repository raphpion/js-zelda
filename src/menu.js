import { playSound } from './audio.js'
import { game } from './game.js'
import { player } from './player.js'
import { ctx, getScale } from './screen.js'
import { screen_items } from './screens/items.js'
import { screen_equipment } from './screens/equipment.js'
import { sprites } from './assets.js'

const menu = {
  cursor: {
    pos: { x: 0, y: 0 },
    source: { x: 179, y: 0 },
    index: 0,
    animation: null,
    space: 0,
    animate: () => {
      let indexLimit = 3
      menu.cursor.index++
      if (menu.cursor.index > indexLimit) menu.cursor.index = 0
      menu.cursor.source.y = menu.cursor.index * 23
    },
    draw: () => {
      ctx.drawImage(
        sprites.menu,
        menu.cursor.source.x,
        menu.cursor.source.y,
        23,
        23,
        menu.screens[menu.screen].spaces[menu.cursor.pos.x + menu.cursor.pos.y * menu.screens[menu.screen].cols].pos.x +
          menu.screens[menu.screen].pos.x,
        menu.screens[menu.screen].spaces[menu.cursor.pos.x + menu.cursor.pos.y * menu.screens[menu.screen].cols].pos.y +
          menu.screens[menu.screen].pos.y,
        23,
        23
      )
    },
    move: (x = 0, y = 0) => {
      if (game.dialogue != null) return
      playSound('cursor')
      menu.cursor.pos.x += x
      menu.cursor.pos.y += y
      if (menu.cursor.pos.x < 0) {
        if (menu.screen == 0) menu.screen = menu.screens.length - 1
        else menu.screen--
        menu.cursor.pos.x = menu.screens[menu.screen].cols - 1
      }
      if (menu.cursor.pos.x >= menu.screens[menu.screen].cols) {
        if (menu.screen >= menu.screens.length - 1) menu.screen = 0
        else menu.screen++
        menu.cursor.pos.x = 0
      }
      if (menu.cursor.pos.y < 0) menu.cursor.pos.y = menu.screens[menu.screen].rows - 1
      if (menu.cursor.pos.y >= menu.screens[menu.screen].rows) menu.cursor.pos.y = 0
      //
      while (
        menu.screens[menu.screen].spaces[menu.cursor.pos.x + menu.cursor.pos.y * menu.screens[menu.screen].cols].skip !=
        null
      ) {
        if (x != 0) menu.cursor.pos.x += x
        if (y != 0) menu.cursor.pos.y += y
        if (menu.cursor.pos.x < 0) {
          if (menu.screen == 0) menu.screen = menu.screens.length - 1
          else menu.screen--
          menu.cursor.pos.x = menu.screens[menu.screen].cols - 1
        }
        if (menu.cursor.pos.x >= menu.screens[menu.screen].cols) {
          if (menu.screen >= menu.screens.length - 1) menu.screen = 0
          else menu.screen++
          menu.cursor.pos.x = 0
        }
        if (menu.cursor.pos.y < 0) menu.cursor.pos.y = menu.screens[menu.screen].rows - 1
        if (menu.cursor.pos.y >= menu.screens[menu.screen].rows) menu.cursor.pos.y = 0
      }
      //
      let i = menu.cursor.pos.x + menu.cursor.pos.y * menu.screens[menu.screen].cols
      let s = menu.screens[menu.screen].spaces
      if (menu.screen == 0 && s[i].item != null && s[i].item.owned) {
        menu.action = 'Info'
      } else if (menu.screen == 1 && s[i].item != null && s[i].item.owned) {
        let equip = [1, 2, 3, 5, 6, 7, 9, 10, 11]
        let info = [0, 4, 8, 12]
        if (equip.includes(i)) menu.action = 'Equip'
        else if (info.includes(i)) menu.action = 'Info'
      } else menu.action = null
    },
  },
  opened: false,
  screen: 0,
  screens: [screen_items, screen_equipment],
  action: null,
  text: null,
  clear: () => {
    playSound('menu_close')
    game.freeze = false
    menu.opened = false
    clearInterval(menu.cursor.animation)
    for (let screen of menu.screens) screen.isInit = false
  },
  draw: () => {
    menu.screens[menu.screen].draw()
    menu.cursor.draw()
    // infobox
    ctx.drawImage(
      sprites.menu,
      0,
      0,
      151,
      27,
      menu.screens[menu.screen].pos.x + 14,
      menu.screens[menu.screen].pos.y + 166,
      151,
      27
    )
    menu.cursor.space =
      menu.screens[menu.screen].spaces[menu.cursor.pos.x + menu.cursor.pos.y * menu.screens[menu.screen].cols]
    if (menu.cursor.space.item != null && menu.cursor.space.item.text != null && menu.cursor.space.item.owned) {
      ctx.fillStyle = 'white'
      ctx.textAlign = 'center'
      ctx.textBaseline = 'top'
      ctx.font = '8pt Ubuntu'
      ctx.fillText(menu.cursor.space.item.text, screen.width / getScale() / 2, menu.screens[menu.screen].pos.y + 174)
    }
  },
  init: () => {
    menu.cursor.space =
      menu.screens[menu.screen].spaces[menu.cursor.pos.x + menu.cursor.pos.y * menu.screens[menu.screen].cols]
    playSound('menu_open')
    game.freeze = true
    menu.opened = true
    for (let screen of menu.screens) if (!screen.isInit) screen.init()
    menu.cursor.animation = setInterval(menu.cursor.animate, 1000 / 4)
    player.setAction('idle')
  },
}

export { menu }
