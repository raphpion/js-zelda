import { controller } from './controller.js'
import { game } from './game.js'
import { ctx, debugMode } from './screen.js'
import { getClosest } from './collisions.js'
import { playSound } from './audio.js'
import { tunics } from './items/tunics.js'
import { swords } from './items/swords.js'
import { sprites } from './assets.js'
import { getVFX } from './classes/VFX.js'
import { shields } from './items/shields.js'
import { boomerang } from './items/boomerang.js'
import { items } from './items/items.js'

const player = {
  hp: 12,
  hpMax: 12,
  mp: null,
  mpMax: null,
  rupees: null,
  rupeesMax: null,
  purse: null,
  speed: 1.25,
  //
  sword: null,
  shield: null,
  tunic: tunics.green,
  item1: null,
  item2: null,
  //
  hitbox: { pos: { x: 10, y: 17 }, width: 12, height: 14 },
  sprite: null,
  interaction: null,
  carrying: null,
  animation: null,
  action: 'idle',
  direction: 'down',
  canMove: true,
  //
  shieldSprite: {
    draw: false,
    pos: { x: 0, y: 0, z: null },
    source: { x: 0, y: 0 },
    width: 0,
    height: 0,
  },
  swordSprite: {
    draw: false,
    slashes: [],
    pos: { x: 0, y: 0, z: null },
    source: { x: 0, y: 0 },
    width: 0,
    height: 0,
    animation: null,
    charge: 0,
    chargeInterval: 0,
    direction: null,
    trailColor: 0, // for sword spin
    sparks: false, // for sword collide
  },
  //
  index: 0,
  source: {
    x: 0,
    y: 0,
  },
  pos: {
    x: 0,
    y: 0,
  },
  //
  animate: (called = false) => {
    // sword shine particles
    if (player.swordSprite.charge > 12 && player.action != 'swordcollide') {
      let x
      let y
      if (player.swordSprite.direction == 'up') {
        let min = player.pos.x + player.swordSprite.pos.x - 2
        let max = player.pos.x + player.swordSprite.pos.x + player.swordSprite.width
        x = Math.floor(Math.random() * (max - min) + min)
        min = player.pos.y + player.swordSprite.pos.y - 5
        max = player.pos.y + player.swordSprite.pos.y - 3
        y = Math.floor(Math.random() * (max - min) + min)
      }
      if (player.swordSprite.direction == 'down') {
        let min = player.pos.x + player.swordSprite.pos.x - 6
        let max = player.pos.x + player.swordSprite.pos.x + player.swordSprite.width - 4
        x = Math.floor(Math.random() * (max - min) + min)
        min = player.pos.y + player.swordSprite.pos.y + player.swordSprite.height - 5
        max = player.pos.y + player.swordSprite.pos.y + player.swordSprite.height - 3
        y = Math.floor(Math.random() * (max - min) + min)
      }
      if (player.swordSprite.direction == 'left') {
        let min = player.pos.y + player.swordSprite.pos.y - 4
        let max = player.pos.y + player.swordSprite.pos.y + player.swordSprite.height - 2
        y = Math.floor(Math.random() * (max - min) + min)
        min = player.pos.x + player.swordSprite.pos.x - 5
        max = player.pos.x + player.swordSprite.pos.x - 3
        x = Math.floor(Math.random() * (max - min) + min)
      }
      if (player.swordSprite.direction == 'right') {
        let min = player.pos.y + player.swordSprite.pos.y - 4
        let max = player.pos.y + player.swordSprite.pos.y + player.swordSprite.height - 2
        y = Math.floor(Math.random() * (max - min) + min)
        min = player.pos.x + player.swordSprite.pos.x + player.swordSprite.width - 5
        max = player.pos.x + player.swordSprite.pos.x + player.swordSprite.width - 3
        x = Math.floor(Math.random() * (max - min) + min)
      }
      getVFX('sword_shine', x, y)
    } else player.swordSprite.counter = 0
    //
    if (player.action == 'sword') {
      let indexLimit
      let dx
      let dy
      let psx // player source x
      let psy // player source y
      let ssx // sword source x
      let ssy = 0 // TODO: change sy according to player's sword
      let h
      let w
      // TODO: set all sword sprite variables in sword object
      if (player.direction == 'up') {
        indexLimit = 7
        ssx = [280, 292, 306, 320, 327, 327, 333, 340]
        psx = [544, 576, 576, 608, 640, 640, 672, 704]
        psy = 64
        w = [12, 14, 14, 7, 6, 6, 7, 12]
        h = [6, 14, 12, 12, 12, 12, 12, 14]
        dx = [20, 16, 15, 11, 8, 8, 4, -4]
        dy = [18, 7, 1, -1, -4, -4, 1, 9]
        player.swordSprite.pos.z = -1
        //
        if (player.shield != null) {
          player.shieldSprite.source.x = 9
          player.shieldSprite.source.y = 0 // TODO: change shield source with shield
          player.shieldSprite.width = 4
          player.shieldSprite.height = 11
          player.shieldSprite.pos.x = player.index == 0 || player.index == 7 ? 24 : 23
          let sdy = [0, 0, -2, -4, -6, -6, -4, -2]
          player.shieldSprite.pos.y = 18 + sdy[player.index]
        }
      }
      if (player.direction == 'down') {
        indexLimit = 9
        ssx = [0, 12, 24, 38, 52, 59, 59, 65, 76, 86]
        psx = [544, 544, 576, 576, 608, 640, 640, 672, 704]
        psy = 0
        w = [12, 12, 14, 14, 7, 6, 6, 7, 10, 12]
        h = [6, 6, 14, 12, 12, 12, 12, 12, 13, 13]
        dx = [2, 2, 2, 3, 12, 17, 17, 19, 18, 20]
        dy = [20, 20, 21, 27, 29, 32, 32, 28, 25, 23]
        player.swordSprite.pos.z = 1
        //
        if (player.shield != null) {
          player.shieldSprite.source.x = 13
          player.shieldSprite.source.y = 0 // TODO: change shield source with shield
          player.shieldSprite.width = 4
          player.shieldSprite.height = 11
          player.shieldSprite.pos.x = player.index < 5 ? 5 : 4
          let sdy = [0, 0, 3, 3, 4, 7, 7, 5, 5, 5, 5]
          player.shieldSprite.pos.y = 11 + sdy[player.index]
        }
      }
      if (player.direction == 'left') {
        indexLimit = 8
        ssx = [189, 195, 202, 216, 228, 228, 240, 252, 266]
        psx = [544, 544, 576, 608, 640, 640, 672, 704, 704]
        psy = 96
        w = [6, 7, 14, 12, 12, 12, 12, 14, 14]
        h = [12, 12, 14, 14, 6, 6, 7, 14, 12]
        dx = [8, 15, 3, -2, -8, -8, -4, -3, 1]
        dy = [8, 8, 7, 7, 20, 20, 23, 23, 29]
        player.swordSprite.pos.z = player.index > 6 ? 1 : -1
        player.shieldSprite.draw = false
      }
      if (player.direction == 'right') {
        indexLimit = 8
        ssx = [98, 104, 111, 125, 137, 137, 149, 161, 175]
        psx = [544, 544, 576, 608, 640, 640, 672, 704, 704]
        psy = 32
        w = [6, 7, 14, 12, 12, 12, 12, 14, 14]
        h = [12, 12, 14, 14, 6, 6, 7, 14, 12]
        dx = [13, 17, 15, 22, 28, 28, 24, 21, 16]
        dy = [8, 8, 7, 7, 20, 20, 23, 23, 29]
        player.swordSprite.pos.z = player.index > 6 ? 1 : -1
        player.shieldSprite.draw = false
      }
      let i = player.index
      if (i > indexLimit) {
        player.swordSprite.draw = false
        player.setAction('idle')
        player.slash()
        if (controller.keys.sword) {
          player.chargeSword()
          player.animate()
          player.swordSprite.chargeInterval = setInterval(player.chargeSword, 1000 / 16)
        }
      } else {
        player.swordSprite.draw = true
        player.source.x = psx[i]
        player.source.y = psy
        player.swordSprite.pos.x = dx[i]
        player.swordSprite.pos.y = dy[i]
        player.swordSprite.source.x = ssx[i]
        player.swordSprite.source.y = ssy
        player.swordSprite.width = w[i]
        player.swordSprite.height = h[i]
        if (!called) player.index++
        if (player.index > indexLimit) {
          player.swordSprite.draw = false
          player.setAction('idle')
          player.slash()
          if (controller.keys.sword) {
            player.chargeSword()
            player.animate(true)
            player.swordSprite.chargeInterval = setInterval(player.chargeSword, 1000 / 16)
          }
        }
      }
    }
    //
    else if (player.action == 'swordspin') {
      player.shieldSprite.draw = false
      player.swordSprite.charge = 0
      let indexLimit = 12
      let dx
      let dy
      let dz
      let psx // player source x
      let psy // player source y
      let ssx // sword source x
      let ssy = 0 // TODO: change sy according to player's sword
      let h
      let w
      let vx // vfx pos x
      let vy // vfx pos y
      let blueTrail = [0, 1, 5, 6, 10, 11]
      player.swordSprite.trailColor = blueTrail.includes(player.index) ? 0 : 1
      if (player.swordSprite.direction == 'up') {
        psx = [928, 960, 928, 992, 992, 0, 0, 992, 992, 0, 0, 928]
        psy = [96, 96, 96, 96, 96, 32, 32, 0, 0, 96, 96, 96]
        ssx = [52, 65, 52, 371, 383, 395, 104, 352, 149, 364, 52, 52]
        w = [7, 7, 7, 12, 12, 7, 7, 12, 12, 7, 7, 7]
        h = [12, 12, 12, 7, 7, 12, 12, 7, 7, 12, 12, 12]
        dx = [5, 11, 5, -4, -4, 8, 16, 24, 24, 17, 9, 5]
        dy = [22, 27, 22, 22, 15, 2, 2, 15, 23, 27, 27, 22]
        dz = [1, 1, 1, 1, 1, -1, -1, 1, 1, 1, 1, 1]
        vx = [null, null, -11, -14, -5, 7, 9, 14, 6, -6, -13, -9]
        vy = [null, null, 1, 1, -12, -18, -5, 8, 9, 10, 6, 1]
        if (player.index == 1)
          getVFX(
            'sword_boom',
            player.pos.x + player.swordSprite.pos.x + 4,
            player.pos.y + player.swordSprite.pos.y + player.swordSprite.height - 2
          )
        else if (player.index > 1)
          getVFX(
            'sword_trail',
            player.pos.x + player.swordSprite.pos.x + vx[player.index],
            player.pos.y + player.swordSprite.pos.y + vy[player.index]
          )
      }
      if (player.swordSprite.direction == 'down') {
        psx = [926, 960, 928, 992, 992, 0, 0, 992, 992, 0, 0, 928]
        psy = [0, 0, 0, 0, 0, 96, 96, 96, 96, 32, 32, 0]
        ssx = [104, 195, 104, 352, 149, 364, 52, 371, 383, 395, 104, 104]
        w = [7, 7, 7, 12, 12, 7, 7, 12, 12, 7, 7, 7]
        h = [12, 12, 12, 7, 7, 12, 12, 7, 7, 12, 12, 12]
        dx = [22, 16, 22, 24, 24, 17, 9, -4, -4, 8, 16, 22]
        dy = [6, 1, 6, 15, 23, 27, 27, 22, 15, 2, 2, 6]
        dz = [-1, -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1]
        vx = [null, null, 7, 8, 6, -6, -13, -18, -5, 7, 9, 7]
        vy = [null, null, 0, 4, 9, 10, 6, -4, -12, -18, -5, -2]
        if (player.index == 1)
          getVFX(
            'sword_boom',
            player.pos.x + player.swordSprite.pos.x - 13,
            player.pos.y + player.swordSprite.pos.y - 14
          )
        else if (player.index > 1)
          getVFX(
            'sword_trail',
            player.pos.x + player.swordSprite.pos.x + vx[player.index],
            player.pos.y + player.swordSprite.pos.y + vy[player.index]
          )
      }
      if (player.swordSprite.direction == 'left') {
        psx = [928, 960, 928, 0, 0, 992, 992, 0, 0, 992, 992, 928]
        psy = [64, 64, 64, 96, 96, 96, 96, 32, 32, 0, 0, 64]
        ssx = [149, 352, 149, 364, 52, 371, 383, 395, 104, 352, 149, 149]
        w = [12, 12, 12, 7, 7, 12, 12, 7, 7, 12, 12, 12]
        h = [7, 7, 7, 12, 12, 7, 7, 12, 12, 7, 7, 7]
        dx = [21, 23, 21, 17, 9, -4, -4, 8, 16, 24, 24, 21]
        dy = [23, 16, 23, 27, 27, 22, 15, 2, 2, 15, 23, 23]
        dz = [1, 1, 1, 1, 1, 1, 1, -1, -1, 1, 1, 1]
        vx = [null, null, 4, -3, -13, -18, -5, 7, 9, 14, 6, 3]
        vy = [null, null, 8, 10, 6, -4, -12, -19, -5, 8, 9, 1]
        if (player.index == 1)
          getVFX(
            'sword_boom',
            player.pos.x + player.swordSprite.pos.x + player.swordSprite.width - 6,
            player.pos.y + player.swordSprite.pos.y - 14
          )
        else if (player.index > 1)
          getVFX(
            'sword_trail',
            player.pos.x + player.swordSprite.pos.x + vx[player.index],
            player.pos.y + player.swordSprite.pos.y + vy[player.index]
          )
      }
      if (player.swordSprite.direction == 'right') {
        psx = [928, 960, 928, 0, 0, 992, 992, 0, 0, 992, 992, 928]
        psy = [32, 32, 32, 32, 32, 0, 0, 96, 96, 96, 96, 32]
        ssx = [383, 371, 383, 395, 104, 352, 149, 364, 52, 371, 383, 383]
        w = [12, 12, 12, 7, 7, 12, 12, 7, 7, 12, 12, 12]
        h = [7, 7, 7, 12, 12, 7, 7, 12, 12, 7, 7, 7]
        dx = [-2, -3, -2, 8, 16, 24, 24, 17, 9, -4, -4, -2]
        dy = [13, 19, 13, 2, 2, 15, 23, 27, 27, 22, 15, 13]
        dz = [-1, 1, -1, -1, -1, 1, 1, 1, 1, 1, 1, -1]
        vx = [null, null, -4, 5, 9, 14, 6, -6, -13, -18, -5, -3]
        vy = [null, null, -11, -16, -5, 8, 9, 10, 6, -4, -12, -8]
        if (player.index == 1)
          getVFX('sword_boom', player.pos.x + player.swordSprite.pos.x - 8, player.pos.y + player.swordSprite.pos.y + 4)
        else if (player.index > 1)
          getVFX(
            'sword_trail',
            player.pos.x + player.swordSprite.pos.x + vx[player.index],
            player.pos.y + player.swordSprite.pos.y + vy[player.index]
          )
      }
      let i = player.index
      player.slash(true)
      if (i == 3) playSound('sword_spin')
      if (i > indexLimit) {
        player.swordSprite.draw = false
        player.setAction('idle')
      } else {
        player.swordSprite.draw = true
        player.source.x = psx[i]
        player.source.y = psy[i]
        player.swordSprite.pos.x = dx[i]
        player.swordSprite.pos.y = dy[i]
        player.swordSprite.pos.z = dz[i]
        player.swordSprite.source.x = ssx[i]
        player.swordSprite.source.y = ssy
        player.swordSprite.width = w[i]
        player.swordSprite.height = h[i]
        player.index++
        if (player.index > indexLimit) {
          player.swordSprite.draw = false
          player.setAction('idle')
        }
      }
    } else if (player.action == 'swordcollide') {
      let psx = []
      let ssx = []
      let ssy = 0 // TODO: change y source according to sword
      let w = []
      let h = []
      let dx = []
      let dy = []
      let indexLimit = 2
      if (player.swordSprite.direction == 'up') {
        psx = [1024, 1056, 1088]
        ssx = [98, 320, 98]
        w = [6, 7, 6]
        h = [12, 12, 12]
        dx = [6, 8, 6]
        dy = [8, 3, 13]
        player.source.y = 96
      }
      if (player.swordSprite.direction == 'down') {
        psx = [1024, 1056, 1024]
        ssx = [59, 52, 59]
        w = [6, 7, 6]
        h = [12, 12, 12]
        dx = [20, 18, 17]
        dy = [21, 28, 25]
        player.source.y = 0
      }
      if (player.swordSprite.direction == 'left') {
        psx = [1024, 1056, 1024]
        ssx = [0, 0, 0]
        w = [12, 12, 12]
        h = [6, 6, 6]
        dx = [0, -2, 5]
        dy = [21, 19, 16]
        player.source.y = 64
      }
      if (player.swordSprite.direction == 'right') {
        psx = [1024, 1056, 1024]
        ssx = [137, 137, 137]
        w = [12, 12, 12]
        h = [6, 6, 6]
        dx = [20, 22, 15]
        dy = [21, 19, 16]
        player.source.y = 32
      }
      player.swordSprite.draw = true
      let i = player.index
      if (i > indexLimit) {
        player.setAction('idle')
        player.swordSprite.sparks = false
      }
      player.source.x = psx[i]
      player.swordSprite.source.x = ssx[i]
      player.swordSprite.source.y = ssy
      player.swordSprite.width = w[i]
      player.swordSprite.height = h[i]
      player.swordSprite.pos.x = dx[i]
      player.swordSprite.pos.y = dy[i]
      player.swordSprite.pos.z = player.swordSprite.direction == 'up' ? -1 : 1
      //
      if (i == 1 && player.swordSprite.sparks) {
        let x
        let y
        if (player.swordSprite.direction == 'down') {
          x = player.pos.x + player.swordSprite.pos.x - 6
          y = player.pos.y + player.swordSprite.pos.y + 4
        }
        if (player.swordSprite.direction == 'up') {
          x = player.pos.x + player.swordSprite.pos.x - 2
          y = player.pos.y + player.swordSprite.pos.y - 6
        }
        if (player.swordSprite.direction == 'right') {
          x = player.pos.x + player.swordSprite.pos.x + 4
          y = player.pos.y + player.swordSprite.pos.y - 4
        }
        if (player.swordSprite.direction == 'left') {
          x = player.pos.x + player.swordSprite.pos.x - 6
          y = player.pos.y + player.swordSprite.pos.y - 4
        }
        getVFX('sword_spark', x, y)
      }
      player.index++
      if (i > indexLimit) {
        player.setAction('idle')
        player.swordSprite.sparks = false
      }
      player.slash()
    }
    //
    else if (player.action == 'looting') {
      player.shieldSprite.draw = false
      player.swordSprite.draw = false
      player.index = 0
      player.source.x = 256
      player.source.y = 32
    }
    //
    else if (player.action == 'boomerangthrow') {
      let indexLimit = 2
      player.source.x = 1120 + player.index * 32
      let i = player.index
      if (player.shield != null && (player.direction == 'up' || player.direction == 'down')) {
        player.shieldSprite.draw = true
        player.shieldSprite.source.y = 0 // TODO: change according to player equipped shield
        player.shieldSprite.height = 11
        player.shieldSprite.width = i == 0 ? 9 : 4
      } else player.shieldSprite.draw = false
      if (player.direction == 'up') {
        player.source.y = 96
        player.shieldSprite.source.x = i == 0 ? 17 : 9
        player.shieldSprite.pos.x = i == 0 ? 18 : 24
        player.shieldSprite.pos.y = 17
        player.shieldSprite.pos.z = -1
        let x = i == 0 ? -1 : 9
        let y = i == 0 ? 7 : -2
        boomerang.pos.x = player.pos.x + x
        boomerang.pos.y = player.pos.y + y
        boomerang.index = 0
      }
      if (player.direction == 'down') {
        player.source.y = 0
        player.shieldSprite.source.x = i == 0 ? 0 : 13
        player.shieldSprite.pos.x = i == 0 ? 6 : 4
        player.shieldSprite.pos.y = i == 0 ? 20 : 18
        player.shieldSprite.pos.z = 1
        let x = i == 0 ? 21 : 9
        let y = i == 0 ? 9 : 27
        boomerang.pos.x = player.pos.x + x
        boomerang.pos.y = player.pos.y + y
        boomerang.index = 3
      }
      if (player.direction == 'left') {
        player.source.y = 64
        let x = i == 0 ? 21 : -2
        let y = i == 0 ? 6 : 17
        boomerang.pos.x = player.pos.x + x
        boomerang.pos.y = player.pos.y + y
        boomerang.index = 3
      }
      if (player.direction == 'right') {
        player.source.y = 32
        let x = i == 0 ? 0 : 22
        let y = i == 0 ? 5 : 17
        boomerang.pos.x = player.pos.x + x
        boomerang.pos.y = player.pos.y + y
        boomerang.index = 0
      }
      player.index++
      if (player.index > indexLimit) {
        boomerang.throw()
        player.setAction('idle')
      }
    } else if (player.action == 'idle') {
      player.index = 0
      if (player.swordSprite.charge > 0) {
        player.source.x = 736
        player.swordSprite.source.y = 0
        player.swordSprite.draw = true
        if (player.shield != null) {
          player.shieldSprite.source.y = 0 // TODO: change according to player equipped shield
          player.shieldSprite.height = 11
        } else player.shieldSprite.draw = false
        if (player.swordSprite.direction == 'down') {
          player.source.y = 0
          player.swordSprite.width = 7
          player.swordSprite.height = 12
          player.swordSprite.source.x = 52
          player.swordSprite.pos.x = 16
          player.swordSprite.pos.y = 26
          player.swordSprite.pos.z = 1
          if (player.shield != null) {
            player.shieldSprite.source.x = 13
            player.shieldSprite.width = 4
            player.shieldSprite.pos.x = 5
            player.shieldSprite.pos.y = 16
            player.shieldSprite.pos.z = 1
            player.shieldSprite.draw = true
          }
        }
        if (player.swordSprite.direction == 'right') {
          player.source.y = 32
          player.swordSprite.width = 12
          player.swordSprite.height = 6
          player.swordSprite.source.x = 137
          player.swordSprite.pos.x = 18
          player.swordSprite.pos.y = 21
          player.swordSprite.pos.z = 1
          player.shieldSprite.draw = false
        }
        if (player.swordSprite.direction == 'up') {
          player.source.y = 64
          player.swordSprite.width = 7
          player.swordSprite.height = 12
          player.swordSprite.source.x = 320
          player.swordSprite.pos.x = 8
          player.swordSprite.pos.y = 5
          player.swordSprite.pos.z = -1
          if (player.shield != null) {
            player.shieldSprite.source.x = 9
            player.shieldSprite.width = 4
            player.shieldSprite.pos.x = 23
            player.shieldSprite.pos.y = 15
            player.shieldSprite.pos.z = 1
            player.shieldSprite.draw = true
          }
        }
        if (player.swordSprite.direction == 'left') {
          player.source.y = 96
          player.swordSprite.width = 12
          player.swordSprite.height = 6
          player.swordSprite.source.x = 228
          player.swordSprite.pos.x = 2
          player.swordSprite.pos.y = 21
          player.swordSprite.pos.z = 1
          player.shieldSprite.draw = false
        }
      } else {
        player.swordSprite.draw = false
        if (player.direction == 'down') player.source.y = 0
        if (player.direction == 'right') player.source.y = 32
        if (player.direction == 'up') player.source.y = 64
        if (player.direction == 'left') player.source.y = 96
        if (player.carrying != null) {
          player.source.x = 352
          player.shieldSprite.draw = false
        } else {
          player.source.x = 0
          //
          if (player.shield != null) {
            player.shieldSprite.draw = true
            player.shieldSprite.source.y = 0 // TODO: change source y with player shield
            player.shieldSprite.height = 11 // TODO: change height with player shield
            if (player.direction == 'down') {
              player.shieldSprite.source.x = 0
              player.shieldSprite.pos.x = 7
              player.shieldSprite.pos.y = 19
              player.shieldSprite.pos.z = 1
              player.shieldSprite.width = 9
            }
            if (player.direction == 'right') {
              player.shieldSprite.source.x = 9
              player.shieldSprite.pos.x = 21
              player.shieldSprite.pos.y = 14
              player.shieldSprite.pos.z = -1
              player.shieldSprite.width = 4
            }
            if (player.direction == 'up') {
              player.shieldSprite.source.x = 17
              player.shieldSprite.pos.x = 17
              player.shieldSprite.pos.y = 15
              player.shieldSprite.pos.z = -1
              player.shieldSprite.width = 9
            }
            if (player.direction == 'left') {
              player.shieldSprite.source.x = 13
              player.shieldSprite.pos.x = 7
              player.shieldSprite.pos.y = 14
              player.shieldSprite.pos.z = -1
              player.shieldSprite.width = 4
            }
          } else player.shieldSprite.draw = false
        }
      }
    }
    //
    else if (player.action == 'walk') {
      let indexLimit
      if (player.swordSprite.charge > 0) {
        player.source.x = 736 + player.index * 32
        player.swordSprite.source.y = 0
        player.swordSprite.draw = true
        if (player.shield != null) {
          player.shieldSprite.pos.y = player.shieldSprite.source.y = 0 // TODO: change with shield
          player.shieldSprite.height = 11
        } else player.shieldSprite.draw = false
        if (player.swordSprite.direction == 'down') {
          indexLimit = 5
          player.source.y = 0
          player.swordSprite.width = 7
          player.swordSprite.height = 12
          player.swordSprite.source.x = 52
          player.swordSprite.pos.x = 16
          let dy = [26, 27, 28, 26, 27, 28]
          player.swordSprite.pos.y = dy[player.index]
          player.swordSprite.pos.z = 1
          if (player.shield != null) {
            player.shieldSprite.source.x = 13
            player.shieldSprite.width = 4
            player.shieldSprite.pos.x = 5
            let sdy = [0, 1, 2, 0, 1, 2]
            player.shieldSprite.pos.y = 16 + sdy[player.index]
            player.shieldSprite.pos.z = 1
            player.shieldSprite.draw = true
          }
        }
        if (player.swordSprite.direction == 'right') {
          indexLimit = 2
          player.source.y = 32
          player.swordSprite.width = 12
          player.swordSprite.height = 6
          player.swordSprite.source.x = 137
          player.swordSprite.pos.x = 18
          player.swordSprite.pos.y = 21 + player.index
          player.swordSprite.pos.z = 1
          player.shieldSprite.draw = false
        }
        if (player.swordSprite.direction == 'up') {
          indexLimit = 5
          player.source.y = 64
          player.swordSprite.width = 7
          player.swordSprite.height = 12
          player.swordSprite.source.x = 320
          player.swordSprite.pos.x = 8
          let dy = [5, 6, 7, 5, 6, 7]
          player.swordSprite.pos.y = dy[player.index]
          player.swordSprite.pos.z = -1
          if (player.shield != null) {
            player.shieldSprite.source.x = 9
            player.shieldSprite.width = 4
            player.shieldSprite.pos.x = 23
            let sdy = [0, 1, 2, 0, 1, 2]
            player.shieldSprite.pos.y = 15 + sdy[player.index]
            player.shieldSprite.pos.z = 1
            player.shieldSprite.draw = true
          }
        }
        if (player.swordSprite.direction == 'left') {
          indexLimit = 2
          player.source.y = 96
          player.swordSprite.width = 12
          player.swordSprite.height = 6
          player.swordSprite.source.x = 228
          player.swordSprite.pos.x = 2
          player.swordSprite.pos.y = 21 + player.index
          player.swordSprite.pos.z = 1
          player.shieldSprite.draw = false
        }
        player.index++
        if (player.index > indexLimit) player.index = 0
      } else {
        if (player.direction == 'down') player.source.y = 0
        if (player.direction == 'right') player.source.y = 32
        if (player.direction == 'up') player.source.y = 64
        if (player.direction == 'left') player.source.y = 96
        if (player.carrying != null) {
          player.shieldSprite.draw = false
          if (player.direction == 'down' || player.direction == 'up') indexLimit = 5
          else indexLimit = 2
          if (!called) player.index++
          if (player.index > indexLimit) player.index = 0
          player.source.x = 352 + player.index * 32
        } else {
          if (player.direction == 'down' || player.direction == 'up') indexLimit = 8
          else indexLimit = 7
          if (!called) player.index++
          if (player.index > indexLimit) player.index = 0
          player.source.x = player.index * 32
          //
          if (player.shield != null) {
            player.shieldSprite.draw = true
            player.shieldSprite.source.y = 0 // TODO: change source y with player shield
            player.shieldSprite.height = 11 // TODO: change height with player shield
            let dx
            let dy
            if (player.direction == 'down') {
              player.shieldSprite.source.x = 0
              player.shieldSprite.pos.x = 7
              dy = [0, 0, -1, -2, 0, 0, -1, -2, 0]
              player.shieldSprite.pos.y = 19 + dy[player.index]
              player.shieldSprite.pos.z = 1
              player.shieldSprite.width = 9
            }
            if (player.direction == 'right') {
              player.shieldSprite.source.x = 9
              dx = [0, 0, 1, 0, 0, 0, 1, 1]
              dy = [0, -1, -1, 0, 0, -1, -1, 0]
              player.shieldSprite.pos.x = 21 + dx[player.index]
              player.shieldSprite.pos.y = 14 + dy[player.index]
              player.shieldSprite.pos.z = -1
              player.shieldSprite.width = 4
            }
            if (player.direction == 'up') {
              player.shieldSprite.source.x = 17
              player.shieldSprite.pos.x = 17
              dy = [0, 0, -1, -2, 0, 0, -1, -2, 0]
              player.shieldSprite.pos.y = 15 + dy[player.index]
              player.shieldSprite.pos.z = -1
              player.shieldSprite.width = 9
            }
            if (player.direction == 'left') {
              player.shieldSprite.source.x = 13
              dx = [0, 0, 1, 0, 0, 0, 1, 1]
              dy = [0, -1, -1, 0, 0, -1, -1, 0]
              player.shieldSprite.pos.x = 7 - dx[player.index]
              player.shieldSprite.pos.y = 14 + dy[player.index]
              player.shieldSprite.pos.z = -1
              player.shieldSprite.width = 4
            }
          } else player.shieldSprite.draw = false
        }
      }
    }
    //
    else if (player.action == 'lifting') {
      player.shieldSprite.draw = false
      let indexLimit = 7
      if (!called) player.index++
      if (player.index > indexLimit) player.index = 0
      player.source.x = 288 + Math.floor(player.index / 4) * 32
      if (player.index == 4) getVFX('player_sweat', player.pos.x, player.pos.y)
      if (player.direction == 'down') player.source.y = 0
      if (player.direction == 'right') player.source.y = 32
      if (player.direction == 'up') player.source.y = 64
      if (player.direction == 'left') player.source.y = 96
    }
  },
  chargeSword: () => {
    if (player.swordSprite.charge == 0) {
      player.index = 0
      player.animate()
    }
    if (player.action != 'swordcollide') player.swordSprite.charge++
    if (player.swordSprite.charge > 12) {
      playSound('sword_charged')
      clearInterval(player.swordSprite.chargeInterval)
      player.swordSprite.chargeInterval = null
      getVFX('sword_ready')
    } else if (player.swordSprite.charge > 4) {
      if (player.swordSprite.direction == 'up' || player.swordSprite.direction == 'down') {
        let min = player.swordSprite.pos.x + player.pos.x - 4
        let max = player.swordSprite.pos.x + player.swordSprite.width + player.pos.x - 4
        let a = player.swordSprite.pos.y + player.swordSprite.height + player.pos.y - player.swordSprite.charge * 1.25
        let b = player.swordSprite.pos.y + player.pos.y - 6 + player.swordSprite.charge * 1.25
        let x = Math.floor(Math.random() * (max - min) + min)
        let y = player.swordSprite.direction == 'up' ? a : b
        getVFX('sword_shine', x, y)
      }
      if (player.swordSprite.direction == 'left' || player.swordSprite.direction == 'right') {
        let min = player.swordSprite.pos.y + player.pos.y - 4
        let max = player.swordSprite.pos.y + player.swordSprite.height + player.pos.y - 4
        let a =
          player.swordSprite.pos.x + player.swordSprite.width + player.pos.x - player.swordSprite.charge * 1.25 - 2
        let b = player.swordSprite.pos.x + player.pos.x + player.swordSprite.charge * 1.25 - 6
        let y = Math.floor(Math.random() * (max - min) + min)
        let x = player.swordSprite.direction == 'left' ? a : b
        getVFX('sword_shine', x, y)
      }
    }
  },
  draw: () => {
    if (player.swordSprite.pos.z == -1) player.drawSword()
    if (player.shieldSprite.pos.z == -1) player.drawShield()
    ctx.drawImage(
      player.sprite,
      player.source.x,
      player.source.y,
      32,
      32,
      Math.round(player.pos.x + game.map.offset.x),
      Math.round(player.pos.y + game.map.offset.y),
      32,
      32
    )
    if (player.shieldSprite.pos.z == 1) player.drawShield()
    if (player.swordSprite.pos.z == 1) player.drawSword()
    if (player.carrying != null) player.carrying.draw()
    if (player.vfx != null) player.vfx.draw()
    if (debugMode()) {
      if (player.interaction == null) ctx.fillStyle = 'rgba(255, 0, 0, 0.5)'
      else ctx.fillStyle = 'rgba(0, 255, 0, 0.5)'
      ctx.fillRect(
        Math.round(player.pos.x + player.hitbox.pos.x + game.map.offset.x),
        Math.round(player.pos.y + player.hitbox.pos.y + game.map.offset.y),
        player.hitbox.width,
        player.hitbox.height
      )
    }
  },
  drawShield: () => {
    if (player.shieldSprite.draw) {
      ctx.drawImage(
        sprites.shield,
        player.shieldSprite.source.x,
        player.shieldSprite.source.y,
        player.shieldSprite.width,
        player.shieldSprite.height,
        Math.round(player.shieldSprite.pos.x + player.pos.x + game.map.offset.x),
        Math.round(player.shieldSprite.pos.y + player.pos.y + game.map.offset.y),
        player.shieldSprite.width,
        player.shieldSprite.height
      )
      if (debugMode()) {
        ctx.fillStyle = 'rgba(255, 127, 0, 0.5)'
        ctx.fillRect(
          Math.round(player.shieldSprite.pos.x + player.pos.x + game.map.offset.x),
          Math.round(player.shieldSprite.pos.y + player.pos.y + game.map.offset.y),
          player.shieldSprite.width,
          player.shieldSprite.height
        )
      }
    }
  },
  drawSword: () => {
    if (player.swordSprite.draw) {
      ctx.drawImage(
        sprites.sword,
        player.swordSprite.source.x,
        player.swordSprite.source.y,
        player.swordSprite.width,
        player.swordSprite.height,
        Math.round(player.swordSprite.pos.x + player.pos.x + game.map.offset.x),
        Math.round(player.swordSprite.pos.y + player.pos.y + game.map.offset.y),
        player.swordSprite.width,
        player.swordSprite.height
      )
      if (debugMode()) {
        ctx.fillStyle = 'rgba(255, 127, 0, 0.5)'
        ctx.fillRect(
          Math.round(player.swordSprite.pos.x + player.pos.x + game.map.offset.x),
          Math.round(player.swordSprite.pos.y + player.pos.y + game.map.offset.y),
          player.swordSprite.width,
          player.swordSprite.height
        )
      }
    }
  },
  giveRupees: amt => {
    let i = 0
    let interval = setInterval(() => {
      if (player.rupees == player.rupeesMax || i == amt) {
        if (i % 3 == 0) playSound('purse_full')
        clearInterval(interval)
      } else {
        i++
        playSound('purse')
        player.rupees++
      }
    }, 100)
  },
  load: () => {
    if (localStorage.hasOwnProperty('hpMax')) {
      player.hpMax = Number(localStorage.getItem('hpMax'))
      player.hp = player.hpMax
    }
    if (localStorage.hasOwnProperty('purse')) {
      player.purse = Number(localStorage.getItem('purse'))
      if (player.purse == 1) player.rupeesMax = 99
      if (localStorage.hasOwnProperty('rupees')) {
        player.rupees = Number(localStorage.getItem('rupees'))
        if (player.rupees > player.rupeesMax) player.rupees = player.rupeesMax
      } else player.rupees = 0
    }
    if (localStorage.hasOwnProperty('tunic')) {
      let t = Number(localStorage.getItem('tunic'))
      if (t == 1) {
        player.tunic = tunics.green
        tunics.green.equipped = true
      }
      // TODO: Zora & Goron Tunics
    }
    if (localStorage.hasOwnProperty('sword')) {
      let s = Number(localStorage.getItem('sword'))
      if (s == 1) {
        player.sword = swords.kokiri
        swords.kokiri.equipped = true
      }
    }
    if (localStorage.hasOwnProperty('shield')) {
      let s = Number(localStorage.getItem('shield'))
      if (s == 1) {
        player.shield = shields.deku
        shields.deku.equipped = true
      }
    }
    if (localStorage.hasOwnProperty('item1')) {
      let i = localStorage.getItem('item1')
      if (items.hasOwnProperty(i)) player.item1 = items[i]
    }
    if (localStorage.hasOwnProperty('item2')) {
      let i = localStorage.getItem('item2')
      if (items.hasOwnProperty(i)) player.item2 = items[i]
    }
  },
  save: () => {
    localStorage.setItem('hpMax', player.hpMax)
    if (player.purse != null) localStorage.setItem('purse', player.purse)
    if (player.rupees != null) localStorage.setItem('rupees', player.rupees)
    if (player.tunic == tunics.green) localStorage.setItem('tunic', 1)
    if (player.sword == swords.kokiri) localStorage.setItem('sword', 1)
    if (player.shield == shields.deku) localStorage.setItem('shield', 1)
    if (player.item1 != null) localStorage.setItem('item1', player.item1.name)
    else if (localStorage.hasOwnProperty('item1')) localStorage.removeItem('item1')
    if (player.item2 != null) localStorage.setItem('item2', player.item2.name)
    else if (localStorage.hasOwnProperty('item2')) localStorage.removeItem('item2')
  },
  slash: (all = false) => {
    if (player.swordSprite.slashes.length > 0) {
      if (all) for (let slash of player.swordSprite.slashes) slash.break()
      else getClosest(player.swordSprite.slashes).break()
      player.swordSprite.slashes = []
    }
  },
  setAction: (action = 'idle', interval = 8) => {
    if (player.animation != null) clearInterval(player.animation)
    if (action == 'idle' || action == 'walk') player.canMove = true
    else player.canMove = false
    player.action = action
    player.index = action == 'walk' ? 1 : 0
    player.animate(true)
    player.animation = setInterval(player.animate, 1000 / interval)
  },
  move: () => {
    if (!player.canMove) return
    else if (controller.mainAxis != null) {
      if (player.action != 'walk') {
        player.setAction('walk')
        clearInterval()
      }
      if (player.direction != controller.mainAxis) {
        player.direction = controller.mainAxis
        player.animate(true)
      }
      if (player.swordSprite.charge > 0) {
        player.pos.x += controller.joystick.x * player.speed * 0.7
        player.pos.y += controller.joystick.y * player.speed * 0.7
      } else {
        player.pos.x += controller.joystick.x * player.speed
        player.pos.y += controller.joystick.y * player.speed
      }
      if (player.carrying != null) {
        player.carrying.pos.x += controller.joystick.x * player.speed
        player.carrying.pos.y += controller.joystick.y * player.speed
      }
    } else player.setAction('idle')
  },
}

export { player }
