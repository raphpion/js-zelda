import { playSound } from './audio.js'
import { game } from './game.js'
import { menu } from './menu.js'
import { player } from './player.js'
import { toggleDebug } from './screen.js'

const controller = {
  mainAxis: null,
  keys: {
    up: false,
    down: false,
    left: false,
    right: false,
    action: false,
    sword: false,
    item1: false,
    item2: false,
  },
  keybindings: {
    up: 38,
    down: 40,
    left: 37,
    right: 39,
    action: 83,
    sword: 68,
    item1: 65,
    item2: 87,
    debug: 104,
  },
  joystick: {
    x: 0,
    y: 0,
  },
  keydown: e => {
    if (e.keyCode == controller.keybindings.up) {
      controller.keys.up = true
      if (controller.keys.down) controller.mainAxis = null
      else if (controller.mainAxis == null || controller.mainAxis == 'down') {
        controller.mainAxis = 'up'
      }
      controller.updateJoystick()
    }
    //
    if (e.keyCode == controller.keybindings.down) {
      controller.keys.down = true
      if (controller.keys.up) controller.mainAxis = null
      else if (controller.mainAxis == null || controller.mainAxis == 'up') {
        controller.mainAxis = 'down'
      }
      controller.updateJoystick()
    }
    //
    if (e.keyCode == controller.keybindings.left) {
      controller.keys.left = true
      if (controller.keys.right) controller.mainAxis = null
      else if (controller.mainAxis == null || controller.mainAxis == 'right') {
        controller.mainAxis = 'left'
      }
      controller.updateJoystick()
    }
    //
    if (e.keyCode == controller.keybindings.right) {
      controller.keys.right = true
      if (controller.keys.left) controller.mainAxis = null
      else if (controller.mainAxis == null || controller.mainAxis == 'left') {
        controller.mainAxis = 'right'
      }
      controller.updateJoystick()
    }
    //
    if (menu.opened) {
      if (e.keyCode == controller.keybindings.up) menu.cursor.move(0, -1)
      if (e.keyCode == controller.keybindings.down) menu.cursor.move(0, 1)
      if (e.keyCode == controller.keybindings.left) menu.cursor.move(-1, 0)
      if (e.keyCode == controller.keybindings.right) menu.cursor.move(1, 0)
    }
    //
    if (e.keyCode == controller.keybindings.action && !controller.keys.action) {
      controller.keys.action = true
      if (game.dialogue != null && game.dialogue.canAdvance) {
        playSound('message_finish')
        game.dialogue.slide++
      } else if (game.dialogue == null && menu.opened) {
        if (menu.action == 'Info') menu.cursor.space.dialogue.init()
        if (menu.action == 'Equip') menu.cursor.space.item.equip()
      } else if (
        !game.freeze &&
        player.action != 'sword' &&
        player.action != 'swordspin' &&
        player.action != 'swordcollide' &&
        player.swordSprite.charge == 0
      ) {
        if (player.action != 'lifting') {
          if (player.carrying != null) player.carrying.throw()
          else if (player.interaction != null) {
            player.interaction.interact()
          }
        }
      }
    }
    //
    if (
      e.keyCode == controller.keybindings.sword &&
      !controller.keys.sword &&
      !game.freeze &&
      player.action != 'lifting' &&
      player.action != 'swordspin' &&
      player.sword != null
    ) {
      if (player.carrying != null) player.carrying.throw()
      else {
        if (player.action != 'idle' && player.action != 'walk') {
          if (player.index < 6) return
          player.slash()
        }
        controller.keys.sword = true
        playSound('sword')
        player.swordSprite.slashes = []
        player.swordSprite.direction = player.direction
        player.setAction('sword', 36)
      }
    }
    //
    if (e.keyCode == controller.keybindings.item1) {
      if (game.map.dialogue != null && !game.map.dialogue.clearing) return
      if (menu.opened && menu.screen == 0 && menu.cursor.space.item.owned) {
        playSound('sword_shine')
        if (player.item2 == menu.cursor.space.item) {
          if (player.item1 != null) player.item2 = player.item1
          else player.item2 = null
        }
        player.item1 = menu.cursor.space.item
      } else if (
        !game.freeze &&
        player.item1 != null &&
        player.item1.ready &&
        (player.action == 'idle' || player.action == 'walk')
      )
        player.item1.use()
    }
    if (e.keyCode == controller.keybindings.item2) {
      if (game.map.dialogue != null && !game.map.dialogue.clearing) return
      if (menu.opened && menu.screen == 0 && menu.cursor.space.item.owned) {
        playSound('sword_shine')
        if (player.item1 == menu.cursor.space.item) {
          if (player.item2 != null) player.item1 = player.item2
          else player.item1 = null
        }
        player.item2 = menu.cursor.space.item
      } else if (
        !game.freeze &&
        player.item2 != null &&
        player.item2.ready &&
        (player.action == 'idle' || player.action == 'walk')
      )
        player.item2.use()
    }
  },
  keyup: e => {
    if (e.keyCode == controller.keybindings.up) {
      controller.keys.up = false
      if (player.swordSprite.direction == 'up' && player.action == 'swordcollide') player.setAction('idle')
      if (controller.keys.left) {
        controller.mainAxis = 'left'
      } else if (controller.keys.right) {
        controller.mainAxis = 'right'
      } else if (controller.keys.down) {
        controller.mainAxis = 'down'
      } else controller.mainAxis = null
      controller.updateJoystick()
    }
    //
    if (e.keyCode == controller.keybindings.down) {
      controller.keys.down = false
      if (player.swordSprite.direction == 'down' && player.action == 'swordcollide') player.setAction('idle')
      if (controller.keys.left) {
        controller.mainAxis = 'left'
      } else if (controller.keys.right) {
        controller.mainAxis = 'right'
      } else if (controller.keys.up) {
        controller.mainAxis = 'up'
      } else controller.mainAxis = null
      controller.updateJoystick()
    }
    //
    if (e.keyCode == controller.keybindings.left) {
      controller.keys.left = false
      if (player.swordSprite.direction == 'left' && player.action == 'swordcollide') player.setAction('idle')
      if (controller.keys.up) {
        controller.mainAxis = 'up'
      } else if (controller.keys.down) {
        controller.mainAxis = 'down'
      } else if (controller.keys.right) {
        controller.mainAxis = 'right'
      } else controller.mainAxis = null
      controller.updateJoystick()
    }
    //
    if (e.keyCode == controller.keybindings.right) {
      controller.keys.right = false
      if (player.swordSprite.direction == 'right' && player.action == 'swordcollide') player.setAction('idle')
      if (controller.keys.up) {
        controller.mainAxis = 'up'
      } else if (controller.keys.down) {
        controller.mainAxis = 'down'
      } else if (controller.keys.left) {
        controller.mainAxis = 'left'
      } else controller.mainAxis = null
      controller.updateJoystick()
    }
    //
    if (e.keyCode == controller.keybindings.action) controller.keys.action = false
    if (e.keyCode == controller.keybindings.sword && !game.freeze) {
      controller.keys.sword = false
      if (player.swordSprite.chargeInterval != null) {
        clearInterval(player.swordSprite.chargeInterval)
        player.swordSprite.chargeInterval = null
      }
      if (player.swordSprite.charge > 12) {
        player.direction = player.swordSprite.direction
        player.setAction('swordspin', 24)
      } else if (player.swordSprite.charge > 0) {
        if (controller.mainAxis == null) player.direction = player.swordSprite.direction
        player.swordSprite.direction = null
        player.swordSprite.charge = 0
        player.swordSprite.draw = false
      }
    }
  },
  keypress: e => {
    if (e.keyCode == controller.keybindings.debug) toggleDebug()
    //* experimental
    if (e.keyCode == 112) game.save()
    if (e.keyCode == 13 && game.dialogue == null) {
      if (menu.opened) menu.clear()
      else if (!game.freeze) menu.init()
    }
  },
  updateJoystick() {
    if (controller.keys.up) {
      controller.keys.down ? (controller.joystick.y = 0) : (controller.joystick.y = -1)
    } else if (controller.keys.down) controller.joystick.y = 1
    else controller.joystick.y = 0

    if (controller.keys.left) {
      controller.keys.right ? (controller.joystick.x = 0) : (controller.joystick.x = -1)
    } else if (controller.keys.right) controller.joystick.x = 1
    else controller.joystick.x = 0

    if (controller.joystick.y != 0 && controller.joystick.x != 0) {
      controller.joystick.y *= 0.7
      controller.joystick.x *= 0.7
    }
  },
}

export { controller }
