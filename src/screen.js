import { game } from './game.js'
import { player } from './player.js'

const screen = document.getElementById('screen')
const ctx = screen.getContext('2d')

screen.oncontextmenu = function (e) {
  e.preventDefault()
  e.stopPropagation()
}

let debug = false,
  currentScene,
  gameInterval,
  scale

const transitions = {
  fadeIn: {
    duration: 0,
    opacity: 1,
    interval: null,
    clear: () => {
      transitions.fadeIn.opacity = 0
      game.transition = null
    },
    draw: () => {
      ctx.globalAlpha = transitions.fadeIn.opacity
      ctx.fillStyle = 'black'
      ctx.fillRect(0, 0, screen.width, screen.height)
      ctx.globalAlpha = 1
    },
    init: t => {
      transitions.fadeIn.duration = t
      transitions.fadeIn.opacity = 1
      game.transition = transitions.fadeIn
      //
      if (transitions.fadeIn.duration == 0) {
        transitions.fadeIn.opacity = 0
      } else
        transitions.fadeIn.interval = setInterval(() => {
          transitions.fadeIn.opacity -= 1 / (60 * (t / 1000))
          //
          if (transitions.fadeIn.opacity <= 0) {
            clearInterval(transitions.fadeIn.interval)
            transitions.fadeIn.clear()
          }
        }, 1000 / 60)
    },
  },
  fadeOut: {
    duration: 0,
    opacity: 0,
    interval: null,
    clear: () => {
      transitions.fadeOut.opacity = 0
      game.transition = null
    },
    draw: () => {
      ctx.globalAlpha = transitions.fadeOut.opacity
      ctx.fillStyle = 'black'
      ctx.fillRect(0, 0, screen.width, screen.height)
      ctx.globalAlpha = 1
    },
    init: t => {
      transitions.fadeOut.duration = t
      transitions.fadeOut.opacity = 0
      game.transition = transitions.fadeOut
      //
      if (transitions.fadeOut.duration == 0) {
        transitions.fadeOut.opacity = 1
      } else
        transitions.fadeOut.interval = setInterval(() => {
          transitions.fadeOut.opacity += 1 / (60 * (t / 1000))
          //
          if (transitions.fadeOut.opacity >= 1) {
            transitions.fadeOut.opacity = 1
            clearInterval(transitions.fadeOut.interval)
          }
        }, 1000 / 60)
    },
  },
}
//
function centerMap() {
  if (game.map.width < screen.width / scale) game.map.offset.x = (screen.width / scale - game.map.width) / 2
  else {
    game.map.offset.x = screen.width / (2 * scale) - 16 - player.pos.x
    if (game.map.offset.x > 0) game.map.offset.x = 0
    if (game.map.offset.x + game.map.width < screen.width / scale)
      game.map.offset.x = screen.width / scale - game.map.width
  }
  if (game.map.height < screen.height / scale) game.map.offset.y = (screen.height / scale - game.map.height) / 2
  else {
    game.map.offset.y = screen.height / (2 * scale) - 16 - player.pos.y
    if (game.map.offset.y > 0) game.map.offset.y = 0
    if (game.map.offset.y + game.map.height < screen.height / scale)
      game.map.offset.y = screen.height / scale - game.map.height
  }
}

function clearScreen() {
  ctx.clearRect(0, 0, screen.width, screen.height)
}

function debugMode() {
  return debug
}

function disableSmoothing() {
  ctx.webkitImageSmoothingEnabled = false
  ctx.mozImageSmoothingEnabled = false
  ctx.imageSmoothingEnabled = false
}

function drawHitboxes(obj) {
  ctx.fillStyle = 'rgba(255, 0, 0, 0.5)'
  for (let hb of obj.hitbox) {
    if (hb.type == 'rect')
      ctx.fillRect(
        Math.round(obj.pos.x + hb.pos.x + game.map.offset.x),
        Math.round(obj.pos.y + hb.pos.y + game.map.offset.y),
        hb.width,
        hb.height
      )
  }
}

function drawBlocks(map) {
  ctx.fillStyle = 'rgba(0, 0, 255, 0.5)'
  for (let block of map.blocks)
    ctx.fillRect(
      Math.round(block.pos.x + game.map.offset.x),
      Math.round(block.pos.y + game.map.offset.y),
      block.width,
      block.height
    )
  ctx.fillStyle = 'rgba(126, 0, 255, 0.5)'
  for (let link of map.mapLinks)
    ctx.fillRect(
      Math.round(link.pos.x + game.map.offset.x),
      Math.round(link.pos.y + game.map.offset.y),
      link.width,
      link.height
    )
}

function getMaxScale() {
  let maxScaleX = Math.floor(screen.width / 400)
  let maxScaleY = Math.floor(screen.height / 400)
  if (maxScaleX == 0 || maxScaleY == 0) return 1
  if (maxScaleX == maxScaleY || maxScaleX > maxScaleY) return maxScaleX
  else return maxScaleY
}

function getScale() {
  return scale
}

function getScene(scene) {
  if (currentScene != null && currentScene != scene) currentScene.clear()
  if (currentScene != scene) gameInterval = scene.init()
}

function resizeScreen() {
  screen.width = window.innerWidth
  screen.height = window.innerHeight
  scale = getMaxScale()
  ctx.scale(scale, scale)
  if (debug) console.log(`Set screen dimensions to ${screen.width}x${screen.height}px. Set scale to ${scale}`)
  disableSmoothing()
}

function toggleDebug() {
  debug ? (debug = false) : (debug = true)
  console.log('Toggling debug mode.')
}

export {
  screen,
  ctx,
  transitions,
  centerMap,
  clearScreen,
  disableSmoothing,
  debugMode,
  drawHitboxes,
  drawBlocks,
  getMaxScale,
  getScale,
  getScene,
  resizeScreen,
  toggleDebug,
}
