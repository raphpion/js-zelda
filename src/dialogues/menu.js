import { Dialogue } from '../classes/Dialogue.js'

const dialogue_menu = {
  greenPurse: new Dialogue(null, 'left'),
  greenTunic: new Dialogue(null, 'left'),
}

let s = [{ text: 'Green Purse', style: '#78b820' }, { text: 'A small purse, it can hold up to 99 rupees.' }]
dialogue_menu.greenPurse.slides.push(s)

s = [{ text: 'Kokiri Tunic', style: '#78b820' }, { text: `Feels like you've had it forever!` }]
dialogue_menu.greenTunic.slides.push(s)

export { dialogue_menu }
