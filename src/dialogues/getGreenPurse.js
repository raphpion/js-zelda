import { Dialogue } from '../classes/Dialogue.js'

const dialogue_getGreenPurse = new Dialogue(null, 'left')

let slide = [{ text: 'You found a Green Purse!' }, { text: 'It can hold up to 99 rupees. Neat!' }]

dialogue_getGreenPurse.slides.push(slide)

export { dialogue_getGreenPurse }
