import { Dialogue } from '../classes/Dialogue.js'

const dialogue_getBoomerang = new Dialogue(null, 'left')

let slide = [
  { text: 'You found the Boomerang!' },
  { text: `Use it to paralyze foes and catch loot.` },
  { text: 'Equip it in the item menu.' },
]

dialogue_getBoomerang.slides.push(slide)

export { dialogue_getBoomerang }
