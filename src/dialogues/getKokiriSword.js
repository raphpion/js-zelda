import { Dialogue } from '../classes/Dialogue.js'

const dialogue_getKokiriSword = new Dialogue(null, 'left')

let slide = [
  { text: 'You found the Kokiri Sword!' },
  { text: `The blade still seems sharp.` },
  { text: 'Equip it in the equipment menu.' },
]

dialogue_getKokiriSword.slides.push(slide)

export { dialogue_getKokiriSword }
