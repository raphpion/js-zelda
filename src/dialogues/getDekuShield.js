import { Dialogue } from '../classes/Dialogue.js'

const dialogue_getDekuShield = new Dialogue(null, 'left')

let slide = [
  { text: 'You found the Deku Shield!' },
  { text: `It will protect you from projectiles.` },
  { text: 'Equip it in the equipment menu.' },
]

dialogue_getDekuShield.slides.push(slide)

export { dialogue_getDekuShield }
