import { Dialogue } from '../classes/Dialogue.js'

const dialogue_chestEmpty = new Dialogue(null, 'center')
let slide = [{ text: 'The chest is empty.. too bad!' }]
dialogue_chestEmpty.slides.push(slide)

export { dialogue_chestEmpty }
