import { Dialogue } from '../classes/Dialogue.js'

const dialogue_boomerang = new Dialogue()

let slide = [
  { text: 'Boomerang', style: 'gold' },
  { text: `Use it to paralyze foes and catch loot.` },
  { text: 'Equip it in any item slot.' },
]

dialogue_boomerang.slides.push(slide)

export { dialogue_boomerang }
