import { Chest } from '../classes/Chest.js'
import { purses } from '../items/purses.js'
import { swords } from '../items/swords.js'
import { shields } from '../items/shields.js'
import { boomerang } from '../items/boomerang.js'
import { dialogue_getDekuShield } from '../dialogues/getDekuShield.js'
import { dialogue_getGreenPurse } from '../dialogues/getGreenPurse.js'
import { dialogue_getKokiriSword } from '../dialogues/getKokiriSword.js'
import { dialogue_getBoomerang } from '../dialogues/getBoomerang.js'

const chests = {
  boomerang: new Chest(392, 368, boomerang, dialogue_getBoomerang),
  deku_shield: new Chest(144, 112, shields.deku, dialogue_getDekuShield),
  green_purse: new Chest(176, 112, purses.green, dialogue_getGreenPurse),
  kokiri_sword: new Chest(160, 112, swords.kokiri, dialogue_getKokiriSword),
  load: () => {
    for (let chest in chests) if (localStorage.hasOwnProperty(`chest_${chest}`)) chests[chest].open = true
  },
  save: () => {
    for (let chest in chests)
      if (chests[chest].open && !localStorage.hasOwnProperty(`chest_${chest}`))
        localStorage.setItem(`chest_${chest}`, true)
  },
}

export { chests }
