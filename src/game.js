import { centerMap, clearScreen, transitions } from './screen.js'
import { controller } from './controller.js'
import { player } from './player.js'
import { chests } from './objects/chests.js'
import { sprites } from './assets.js'
import { map_links_house } from './maps/links_house.js'
import { hud } from './hud.js'
import { menu } from './menu.js'
import { purses } from './items/purses.js'
import { tunics } from './items/tunics.js'
import { swords } from './items/swords.js'
import { handleCollision } from './collisions.js'
import { shields } from './items/shields.js'
import { items } from './items/items.js'

const game = {
  map: null,
  freeze: false,
  dialogue: null,
  transition: null,
  clear: () => {},
  draw: () => {
    clearScreen()
    if (!game.freeze) {
      player.move()
      handleCollision()
    }
    centerMap()
    if (game.map != null) game.map.draw()
    if (menu.opened) menu.draw()
    hud.draw()
    if (game.dialogue != null) game.dialogue.draw()
    if (game.transition != null) game.transition.draw()
  },
  init: () => {
    game.freeze = true
    transitions.fadeOut.init(0)
    player.sprite = sprites.link_green
    player.direction = 'down'
    player.setAction('idle')
    map_links_house.init()
    player.pos.x = 64
    player.pos.y = 16
    //
    document.addEventListener('keydown', controller.keydown)
    document.addEventListener('keyup', controller.keyup)
    document.addEventListener('keypress', controller.keypress)
    //

    setTimeout(() => {
      transitions.fadeIn.init(600)
    }, 600)
    setTimeout(() => {
      game.freeze = false
    }, 600)
    return setInterval(game.draw, 1000 / 60)
  },
  load: () => {
    player.load()
    chests.load()
    purses.load()
    tunics.load()
    swords.load()
    shields.load()
    items.load()
  },
  save: () => {
    player.save()
    chests.save()
    purses.save()
    tunics.save()
    swords.save()
    shields.save()
    items.save()
    console.log('Saved game succesfully!')
  },
}

export { game }
