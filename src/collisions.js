import { playSound } from './audio.js'
import { getVFX } from './classes/VFX.js'
import { controller } from './controller.js'
import { game } from './game.js'
import { boomerang } from './items/boomerang.js'
import { player } from './player.js'

function handleCollision() {
  let collisions = []
  let interactions = []
  let sides = {
    top: false,
    down: false,
    left: false,
    right: false,
  }
  let phb = {
    pos: {
      x: player.pos.x + player.hitbox.pos.x,
      y: player.pos.y + player.hitbox.pos.y,
    },
    width: player.hitbox.width,
    height: player.hitbox.height,
  }
  player.interaction = null
  //* 1. check for blocking collision
  for (let block of game.map.blocks) {
    if (detectCollision(block, phb)) {
      let col = getCollisionSide(block, phb)
      if (
        player.action == 'walk' &&
        player.swordSprite.charge > 0 &&
        ((col.ad == 'top' && player.swordSprite.direction == 'down' && controller.joystick.y > 0) ||
          (col.ad == 'bottom' && player.swordSprite.direction == 'up' && controller.joystick.y < 0) ||
          (col.ad == 'left' && player.swordSprite.direction == 'right' && controller.joystick.x > 0) ||
          (col.ad == 'right' && player.swordSprite.direction == 'left' && controller.joystick.x < 0))
      ) {
        player.setAction('swordcollide', 16)
        playSound('tink')
        player.swordSprite.sparks = true
      }
      collisions.push(col)
    }
  }
  //
  for (let destructible of game.map.destructibles) {
    let obj = {
      pos: {
        x: destructible.pos.x + destructible.hitbox.pos.x,
        y: destructible.pos.y + destructible.hitbox.pos.y,
      },
      width: destructible.hitbox.width,
      height: destructible.hitbox.height,
    }
    if (detectCollision(obj, phb)) {
      if (detectInteraction(obj)) {
        interactions.push(destructible)
      }
      let col = getCollisionSide(obj, phb)
      if (
        player.action == 'walk' &&
        player.swordSprite.charge > 0 &&
        ((col.ad == 'top' && player.swordSprite.direction == 'down' && controller.joystick.y > 0) ||
          (col.ad == 'bottom' && player.swordSprite.direction == 'up' && controller.joystick.y < 0) ||
          (col.ad == 'left' && player.swordSprite.direction == 'right' && controller.joystick.x > 0) ||
          (col.ad == 'right' && player.swordSprite.direction == 'left' && controller.joystick.x < 0))
      ) {
        player.setAction('swordcollide', 16)
        if (!destructible.slashable) playSound('tink')
        player.swordSprite.sparks = true
      }
      collisions.push(col)
    }
  }
  //
  for (let object of game.map.objects) {
    let obj = {
      pos: {
        x: object.pos.x + object.hitbox.pos.x,
        y: object.pos.y + object.hitbox.pos.y,
      },
      width: object.hitbox.width,
      height: object.hitbox.height,
    }
    if (detectCollision(obj, phb)) {
      if (detectInteraction(obj, object.interactionDirection) && !object.open) interactions.push(object)
      let col = getCollisionSide(obj, phb)
      if (
        player.action == 'walk' &&
        player.swordSprite.charge > 0 &&
        ((col.ad == 'top' && player.swordSprite.direction == 'down' && controller.joystick.y > 0) ||
          (col.ad == 'bottom' && player.swordSprite.direction == 'up' && controller.joystick.y < 0) ||
          (col.ad == 'left' && player.swordSprite.direction == 'right' && controller.joystick.x > 0) ||
          (col.ad == 'right' && player.swordSprite.direction == 'left' && controller.joystick.x < 0))
      ) {
        player.setAction('swordcollide', 16)
        playSound('tink')
        player.swordSprite.sparks = true
      }
      collisions.push(col)
    }
  }
  player.interaction = getClosest(interactions)
  // apply collision effects
  //? WEIRD BEHAVIOR ON HITBOX INTERSECTS WITH VERTICAL COLLISIONS...
  if (collisions.length > 0) {
    // sort array by collision distance
    collisions = collisions.sort(({ cd: a }, { cd: b }) => b - a)
    let tr = 8 // glide treshold
    // place player along axis for a max of once per side
    for (let obj of collisions) {
      if (obj.ad == 'top' && !sides.top) {
        sides.top = true
        player.pos.y -= obj.cd
        if (player.carrying != null) player.carrying.pos.y -= obj.cd
        if (collisions.length == 1) {
          // Glide player
          if (phb.pos.x + phb.width <= obj.corners.x1 + tr && controller.joystick.x == 0 && controller.joystick.y > 0) {
            player.pos.x--
            if (player.carrying != null) player.carrying.pos.x--
          } else if (phb.pos.x >= obj.corners.x2 - tr && controller.joystick.x == 0 && controller.joystick.y > 0) {
            player.pos.x++
            if (player.carrying != null) player.carrying.pos.x++
          }
        }
      }
      if (obj.ad == 'bottom' && !sides.bottom) {
        sides.bottom = true
        player.pos.y += obj.cd
        if (player.carrying != null) player.carrying.pos.y += obj.cd
        if (collisions.length == 1) {
          // Glide player
          if (phb.pos.x + phb.width <= obj.corners.x1 + tr && controller.joystick.x == 0 && controller.joystick.y < 0) {
            player.pos.x--
            if (player.carrying != null) player.carrying.pos.x--
          } else if (phb.pos.x >= obj.corners.x2 - tr && controller.joystick.x == 0 && controller.joystick.y < 0) {
            player.pos.x++
            if (player.carrying != null) player.carrying.pos.x++
          }
        }
      }
      if (obj.ad == 'left' && !sides.left) {
        sides.left = true
        player.pos.x -= obj.cd
        if (player.carrying != null) player.carrying.pos.x -= obj.cd
        if (collisions.length == 1) {
          // Glide player
          if (
            phb.pos.y + phb.height <= obj.corners.y1 + tr &&
            controller.joystick.y == 0 &&
            controller.joystick.x > 0
          ) {
            player.pos.y--
            if (player.carrying != null) player.carrying.pos.y--
          } else if (phb.pos.y >= obj.corners.y2 - tr && controller.joystick.y == 0 && controller.joystick.x > 0) {
            player.pos.y++
            if (player.carrying != null) player.carrying.pos.y++
          }
        }
      }
      if (obj.ad == 'right' && !sides.right) {
        sides.right = true
        player.pos.x += obj.cd
        if (player.carrying != null) player.carrying.pos.x += obj.cd
        if (collisions.length == 1) {
          // Glide player
          if (
            phb.pos.y + phb.height <= obj.corners.y1 + tr &&
            controller.joystick.y == 0 &&
            controller.joystick.x < 0
          ) {
            player.pos.y--
            if (player.carrying != null) player.carrying.pos.y--
          } else if (phb.pos.y >= obj.corners.y2 - tr && controller.joystick.y == 0 && controller.joystick.x < 0) {
            player.pos.y++
            if (player.carrying != null) player.carrying.pos.y++
          }
        }
      }
    }
  }
  //* 2. Check for loot collision
  for (let loot of game.map.loot) {
    let obj = {
      pos: {
        x: loot.pos.x + loot.hitbox.pos.x,
        y: loot.pos.y + loot.hitbox.pos.y,
      },
      width: loot.hitbox.width,
      height: loot.hitbox.height,
    }
    if (detectCollision(obj, phb)) loot.collide()
  }
  //* 3. Check for maplink collision
  for (let mapLink of game.map.mapLinks) if (detectCollision(mapLink, phb)) mapLink.collide()
  //* 4. Check for sword collision
  if (player.action == 'sword' || player.action == 'swordspin' || player.action == 'swordcollide') {
    for (let destructible of game.map.destructibles) {
      if (destructible.slashable && player.swordSprite.slashes.indexOf(destructible) == -1) {
        let obj = {
          pos: {
            x: destructible.pos.x + destructible.hitbox.pos.x,
            y: destructible.pos.y + destructible.hitbox.pos.y,
          },
          width: destructible.hitbox.width,
          height: destructible.hitbox.height,
        }
        let sword = {
          pos: {
            x: player.swordSprite.pos.x + player.pos.x,
            y: player.swordSprite.pos.y + player.pos.y,
          },
          width: player.swordSprite.width,
          height: player.swordSprite.height,
        }
        let d = getDistance(obj, phb)
        if (detectCollision(obj, sword) && Math.hypot(d.x, d.y) <= 24) {
          player.swordSprite.slashes.push(destructible)
        }
      }
    }
  }
  //* 5. Check for boomerang collision
  if (game.map.projectiles.friendly.indexOf(boomerang) != -1 && boomerang.thrown) {
    if (boomerang.returning) {
      let d = getDistance(boomerang, phb)
      if (d.x < boomerang.speed && d.y < boomerang.speed) boomerang.clear()
    } else {
      for (let loot of game.map.loot) {
        let obj = {
          pos: {
            x: loot.pos.x + loot.hitbox.pos.x,
            y: loot.pos.y + loot.hitbox.pos.y,
          },
          width: loot.hitbox.width,
          height: loot.hitbox.height,
        }
        if (detectCollision(obj, boomerang) && boomerang.loot.indexOf(loot) == -1) {
          boomerang.loot.push(loot)
          loot.pos.x = boomerang.pos.x + boomerang.width / 2 - loot.width / 2 - loot.hitbox.pos.x
          loot.pos.y = boomerang.pos.y + boomerang.height / 2 - loot.height / 2 - loot.hitbox.pos.y
        }
      }
      for (let block of game.map.blocks) {
        if (detectCollision(block, boomerang) && block.blockProjectiles) {
          getVFX('sword_spark', boomerang.pos.x, boomerang.pos.y)
          playSound('tink')
          boomerang.returning = true
        }
      }
    }
  }
}

function detectCollision(obj1, obj2) {
  let d = getDistance(obj1, obj2)
  return d.x <= (obj1.width + obj2.width) / 2 && d.y <= (obj1.height + obj2.height) / 2
}

function detectInteraction(obj, dir = null) {
  // return if player can interact with object
  let phb = {
    pos: {
      x: player.pos.x + player.hitbox.pos.x,
      y: player.pos.y + player.hitbox.pos.y,
    },
    width: player.hitbox.width,
    height: player.hitbox.height,
  }
  let col = getCollisionSide(obj, phb)
  if (
    (((col.ad == 'bottom' && player.direction == 'up') || (col.ad == 'top' && player.direction == 'down')) &&
      phb.pos.x >= obj.pos.x - 8 &&
      phb.pos.x <= obj.pos.x + obj.width - 8) ||
    (((col.ad == 'left' && player.direction == 'right') || (col.ad == 'right' && player.direction == 'left')) &&
      phb.pos.y >= obj.pos.y - 8 &&
      phb.pos.y <= obj.pos.y + obj.height - 8 &&
      (dir == null || player.direction == dir))
  )
    return true
  else return false
}

function getClosest(arr = []) {
  // returns which object is closer to player
  let phb = {
    pos: {
      x: player.pos.x + player.hitbox.pos.x,
      y: player.pos.y + player.hitbox.pos.y,
    },
    width: player.hitbox.width,
    height: player.hitbox.height,
  }
  if (arr.length < 1) return
  if (arr.length == 1) return arr[0]
  let closest = arr[0]
  for (let object of arr) {
    let obj1 = {
      pos: {
        x: closest.pos.x + closest.hitbox.pos.x,
        y: closest.pos.y + closest.hitbox.pos.y,
      },
      width: closest.hitbox.width,
      height: closest.hitbox.height,
    }
    let obj2 = {
      pos: {
        x: object.pos.x + object.hitbox.pos.x,
        y: object.pos.y + object.hitbox.pos.y,
      },
      width: object.hitbox.width,
      height: object.hitbox.height,
    }
    let d1 = getDistance(obj1, phb)
    let dt1 = Math.hypot(d1.x, d1.y)
    let d2 = getDistance(obj2, phb)
    let dt2 = Math.hypot(d2.x, d2.y)
    if (dt2 < dt1) closest = object
  }
  return closest
}

function getCollisionSide(obj1, obj2) {
  // input two hitboxes and return the side of obj1 which obj2 goes over
  let obj = {
    ad: null, // axis direction
    cd: null, // crossdistance
    corners: {
      x1: obj1.pos.x,
      x2: obj1.pos.x + obj1.width,
      y1: obj1.pos.y,
      y2: obj1.pos.y + obj1.height,
    },
  }
  let cw = { left: 0, right: 0 } // crosswidths
  let ch = { top: 0, bottom: 0 } // crossheights
  let cs = { x: null, y: null } // collision sides
  let cd = { da: null, x: 0, y: 0 } // crossdistance
  // get crosswidths
  cw.left = obj2.pos.x + obj2.width - obj1.pos.x
  cw.right = obj1.pos.x + obj1.width - obj2.pos.x
  if (cw.left == cw.right) {
    cs.x = 'center'
    cd.x = cw.left
  } else if (cw.left < cw.right) {
    cs.x = 'left'
    cd.x = cw.left
  } else {
    cs.x = 'right'
    cd.x = cw.right
  }
  // get crossheights
  ch.top = obj2.pos.y + obj2.height - obj1.pos.y
  ch.bottom = obj1.pos.y + obj1.height - obj2.pos.y
  if (ch.top == ch.bottom) {
    cs.y = 'center'
    cd.y = ch.top
  } else if (ch.top < ch.bottom) {
    cs.y = 'top'
    cd.y = ch.top
  } else {
    cs.y = 'bottom'
    cd.y = ch.bottom
  }
  // get dominant axis
  if (cd.x < cd.y) {
    cd.da = 'x'
  } else {
    cd.da = 'y'
  }
  // prepare object
  obj.ad = cd.da == 'x' ? cs.x : cs.y
  obj.cd = cd.da == 'x' ? cd.x : cd.y
  return obj
}

function getDistance(obj1, obj2) {
  // input two objects and return distance
  let c1 = {
    x: obj1.pos.x + obj1.width / 2,
    y: obj1.pos.y + obj1.height / 2,
  }
  let c2 = {
    x: obj2.pos.x + obj2.width / 2,
    y: obj2.pos.y + obj2.height / 2,
  }
  let d = {
    x: Math.abs(c2.x - c1.x),
    y: Math.abs(c2.y - c1.y),
  }
  return d
}

export { getClosest, handleCollision }
