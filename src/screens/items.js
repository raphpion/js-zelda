import { ctx, getScale, screen, debugMode } from '../screen.js'
import { sprites } from '../assets.js'
import { boomerang } from '../items/boomerang.js'
import { dialogue_boomerang } from '../dialogues/boomerang.js'

const screen_items = {
  pos: { x: 0, y: 0 },
  cols: 5,
  rows: 4,
  spaces: [],
  isInit: false,
  draw: () => {
    ctx.drawImage(sprites.menu, 0, 27, 179, 162, screen_items.pos.x, screen_items.pos.y, 179, 162)
    ctx.fillStyle = 'white'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'top'
    ctx.font = '10pt Ubuntu'
    ctx.fillText(
      String.fromCharCode(8678) + ' Items ' + String.fromCharCode(8680),
      screen.width / getScale() / 2,
      screen_items.pos.y + 4
    )
    ctx.fillStyle = 'red'
    if (debugMode())
      for (let rect of screen_items.spaces)
        ctx.fillRect(screen_items.pos.x + rect.pos.x, screen_items.pos.y + rect.pos.y, 23, 23)
    // item slots
    for (let i = 0; i < screen_items.spaces.length; i++) {
      let space = screen_items.spaces[i]
      if (i != 4 && i != 8 && i <= 11 && space.item != null && space.item.owned)
        space.item.draw(screen_items.pos.x + space.pos.x, screen_items.pos.y + space.pos.y, false)
    }
  },
  init: () => {
    screen_items.pos.x = Math.round(screen.width / getScale() / 2 - 89)
    screen_items.pos.y = Math.round(screen.height / getScale() / 2 - 93)
    screen_items.spaces = []
    screen_items.spaces.push(
      { pos: { x: 16, y: 28 }, item: null },
      { pos: { x: 47, y: 28 }, item: boomerang, dialogue: dialogue_boomerang },
      { pos: { x: 78, y: 28 }, item: null },
      { pos: { x: 109, y: 28 }, item: null },
      { pos: { x: 140, y: 28 }, item: null },
      { pos: { x: 16, y: 59 }, item: null },
      { pos: { x: 47, y: 59 }, item: null },
      { pos: { x: 78, y: 59 }, item: null },
      { pos: { x: 109, y: 59 }, item: null },
      { pos: { x: 140, y: 59 }, item: null },
      { pos: { x: 16, y: 90 }, item: null },
      { pos: { x: 47, y: 90 }, item: null },
      { pos: { x: 78, y: 90 }, item: null },
      { pos: { x: 109, y: 90 }, item: null },
      { pos: { x: 140, y: 90 }, item: null },
      { pos: { x: 16, y: 121 }, item: null },
      { pos: { x: 47, y: 121 }, item: null },
      { pos: { x: 78, y: 121 }, item: null },
      { pos: { x: 109, y: 121 }, item: null },
      { pos: { x: 140, y: 121 }, item: null }
    )
    screen_items.isInit = true
  },
}

export { screen_items }
