import { ctx, getScale, screen, debugMode } from '../screen.js'
import { sprites } from '../assets.js'
import { player } from '../player.js'
import { purses } from '../items/purses.js'
import { tunics } from '../items/tunics.js'
import { dialogue_menu } from '../dialogues/menu.js'
import { swords } from '../items/swords.js'
import { shields } from '../items/shields.js'

const screen_equipment = {
  pos: { x: 0, y: 0 },
  cols: 4,
  rows: 4,
  spaces: [],
  isInit: false,
  draw: () => {
    if (!screen_equipment.isInit) return
    ctx.drawImage(sprites.menu, 0, 189, 179, 162, screen_equipment.pos.x, screen_equipment.pos.y, 179, 162)
    ctx.fillStyle = 'white'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'top'
    ctx.font = '10pt Ubuntu'
    ctx.fillText(
      String.fromCharCode(8678) + ' Equipment ' + String.fromCharCode(8680),
      screen.width / getScale() / 2,
      screen_equipment.pos.y + 4
    )
    ctx.fillStyle = 'red'
    if (debugMode())
      for (let rect of screen_equipment.spaces)
        if (rect.pos.x != null && rect.pos.y != null)
          ctx.fillRect(screen_equipment.pos.x + rect.pos.x, screen_equipment.pos.y + rect.pos.y, 23, 23)
    // purse
    // TODO: other purses
    // TODO: quiver
    // TODO: bomb bag
    // TODO: other slot
    // TODO: gloves
    // equipment slots
    for (let i = 0; i < screen_equipment.spaces.length; i++) {
      let space = screen_equipment.spaces[i]
      if (i != 4 && i != 8 && i <= 11 && space.item != null && space.item.owned)
        space.item.draw(screen_equipment.pos.x + space.pos.x, screen_equipment.pos.y + space.pos.y)
    }
    // preview
    ctx.drawImage(
      sprites.link_green,
      0,
      0,
      32,
      32,
      screen_equipment.pos.x + screen_equipment.spaces[15].pos.x - 4.5,
      screen_equipment.pos.y + screen_equipment.spaces[15].pos.y - 8.5,
      32,
      32
    )
    //
    let sx
    let sy
    let dx
    let dy
    let h
    let w
    if (player.sword != null) {
      if (player.sword == swords.kokiri) {
        sx = 189
        sy = 0
        w = 6
        h = 12
        dx = 14.5
        dy = 4.5
      }
      ctx.drawImage(
        sprites.sword,
        sx,
        sy,
        w,
        h,
        screen_equipment.pos.x + screen_equipment.spaces[15].pos.x + dx,
        screen_equipment.pos.y + screen_equipment.spaces[15].pos.y + dy,
        w,
        h
      )
    }
    if (player.shield != null) {
      if (player.shield == shields.deku) {
        sx = 0
        sy = 0
        w = 9
        h = 11
        dx = 2.5
        dy = 10.5
      }
    }
    ctx.drawImage(
      sprites.shield,
      sx,
      sy,
      w,
      h,
      screen_equipment.pos.x + screen_equipment.spaces[15].pos.x + dx,
      screen_equipment.pos.y + screen_equipment.spaces[15].pos.y + dy,
      w,
      h
    )
  },
  init: () => {
    screen_equipment.pos.x = Math.round(screen.width / getScale() / 2 - 89)
    screen_equipment.pos.y = Math.round(screen.height / getScale() / 2 - 93)
    //
    let purse = null
    let dialogue_purse = null
    if (player.purse == 1) {
      purse = purses.green
      dialogue_purse = dialogue_menu.greenPurse
    }
    //
    screen_equipment.spaces = []
    screen_equipment.spaces.push(
      { pos: { x: 16, y: 28 }, item: purse, dialogue: dialogue_purse },
      { pos: { x: 78, y: 28 }, item: swords.kokiri },
      { pos: { x: 109, y: 28 }, item: null },
      { pos: { x: 140, y: 28 }, item: null },
      { pos: { x: 16, y: 59 }, item: null },
      { pos: { x: 78, y: 59 }, item: shields.deku },
      { pos: { x: 109, y: 59 }, item: null },
      { pos: { x: 140, y: 59 }, item: null },
      { pos: { x: 16, y: 90 }, item: null },
      { pos: { x: 78, y: 90 }, item: tunics.green },
      { pos: { x: 109, y: 90 }, item: tunics.blue },
      { pos: { x: 140, y: 90 }, item: tunics.red },
      { pos: { x: 16, y: 121 }, item: null },
      { pos: { x: 78, y: 121 }, item: null, skip: true },
      { pos: { x: 109, y: 121 }, item: null, skip: true },
      { pos: { x: 140, y: 121 }, item: null, skip: true }
    )
    screen_equipment.isInit = true
  },
}

export { screen_equipment }
