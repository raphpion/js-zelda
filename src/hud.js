import { screen, ctx, getScale } from './screen.js'
import { player } from './player.js'
import { sprites } from './assets.js'
import { menu } from './menu.js'
import { game } from './game.js'

const hud = {
  draw: () => {
    hud.drawButtons()
    hud.drawHearts()
    if (player.mpMax != null) hud.drawMP()
    if (player.purse != null) hud.drawRupees()
  },
  drawButtons: () => {
    if (
      (!menu.opened && ((player.interaction == null && player.carrying == null) || player.action == 'lifting')) ||
      (menu.opened && menu.action == null) ||
      (game.dialogue != null && !game.dialogue.canAdvance && !game.dialogue.clearing)
    )
      ctx.globalAlpha = 0.4
    else ctx.globalAlpha = 1
    ctx.drawImage(sprites.hud, 0, 21, 23, 23, screen.width / getScale() - 46, 41, 23, 23)
    ctx.globalAlpha = 1
    ctx.fillStyle = 'white'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'top'
    ctx.font = '6pt Ubuntu'
    if (game.dialogue != null && !game.dialogue.clearing) {
      if (game.dialogue.canAdvance) ctx.fillText('OK', screen.width / getScale() - 34.5, 49)
    } else if (menu.opened && menu.action != null) ctx.fillText(menu.action, screen.width / getScale() - 34.5, 49)
    else if (player.carrying && player.action != 'lifting') ctx.fillText('Throw', screen.width / getScale() - 34.5, 49)
    else if (player.interaction != null) ctx.fillText(player.interaction.text, screen.width / getScale() - 34.5, 49)
    if (player.sword != null) ctx.globalAlpha = 1
    else ctx.globalAlpha = 0.4
    ctx.drawImage(sprites.hud, 23, 21, 23, 23, screen.width / getScale() - 28, 23, 23, 23)
    if (player.sword != null) player.sword.draw(screen.width / getScale() - 28, 23, true)
    //
    if (player.item1 != null) ctx.globalAlpha = 1
    else ctx.globalAlpha = 0.4
    ctx.drawImage(sprites.hud, 0, 44, 23, 23, screen.width / getScale() - 64, 23, 23, 23)
    if (player.item1 != null) player.item1.draw(screen.width / getScale() - 64, 23)
    //
    if (player.item2 != null) ctx.globalAlpha = 1
    else ctx.globalAlpha = 0.4
    ctx.drawImage(sprites.hud, 23, 44, 23, 23, screen.width / getScale() - 46, 5, 23, 23)
    if (player.item2 != null) player.item2.draw(screen.width / getScale() - 46, 5)
    ctx.globalAlpha = 1
  },
  drawHearts: () => {
    for (let i = 0; i < player.hpMax / 4; i++) {
      if (player.hpMax >= (i + 1) * 4) {
        let sx
        let sy = 0
        let dx
        let dy
        if (i < 10) dx = 5 + i * 10
        else dx = 5 + (i - 10) * 10
        if (i < 10) dy = 5
        else dy = 15
        if (player.hp >= (i + 1) * 4) sx = 36
        else if (player.hp == (i + 1) * 4 - 1) sx = 27
        else if (player.hp == (i + 1) * 4 - 2) sx = 18
        else if (player.hp == (i + 1) * 4 - 3) sx = 9
        else sx = 0
        ctx.drawImage(sprites.hud, sx, sy, 9, 9, dx, dy, 9, 9)
      }
    }
  },
  drawMP: () => {
    let barLength
    if (player.mpMax == 20) barLength = 49
    else if (player.mpMax == 40) barLength = 99
    for (let i = 0; i < barLength; i++) {
      let dx = 5
      let dy
      dy = player.hpMax >= 44 ? (dy = 27) : 16
      let sx
      let sy = 9
      //
      if (i == 0) sx = 36
      else if (i == 1) sx = 37
      else if (i == barLength - 2) sx = 40
      else if (i == barLength - 1) sx = 41
      else if (player.mp >= Math.round((i * player.mpMax) / (barLength - 4))) sx = 39
      else sx = 38
      //
      ctx.drawImage(sprites.hud, sx, sy, 1, 8, dx + i, dy, 1, 8)
    }
  },
  drawRupees: () => {
    let sx
    let sy = 9
    let dx = 5
    let dy = screen.height / getScale() - 17
    if (player.purse == 3) sx = 24
    else if (player.purse == 2) sx = 12
    else sx = 0
    ctx.drawImage(sprites.hud, sx, sy, 12, 12, dx, dy, 12, 12)
    //
    let filler
    if (player.rupeesMax < 100) {
      filler = player.rupees < 10 ? '0' : ''
    } else if (player.rupeesMax < 1000) {
      if (player.rupees < 10) filler = '00'
      else filler = player.rupees < 100 ? '0' : ''
    }
    //
    ctx.font = 'italic 10pt Ubuntu'
    if (player.rupees == player.rupeesMax) ctx.fillStyle = '#30c830'
    else ctx.fillStyle = 'white'
    ctx.strokeStyle = 'black'
    ctx.lineWidth = 0.75
    ctx.textAlign = 'left'
    ctx.textBaseline = 'top'
    ctx.fillText(filler + player.rupees, dx + 13, dy)
  },
}

export { hud }
