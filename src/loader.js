import { maps, music, sfx, sprites } from './assets.js'
import { getScene } from './screen.js'
import { game } from './game.js'
import { player } from './player.js'

async function loadAssets() {
  await loadMaps()
  await loadMusic()
  await loadSFX()
  await loadSprites()
  console.log('Loaded assets succesfully!')
  game.load()
  getScene(game)
}

async function loadMaps() {
  const promises = Object.keys(maps).map(layer => {
    return new Promise((resolve, reject) => {
      maps[layer].src = `../assets/maps/${layer}.png`
      maps[layer].addEventListener('load', resolve)
    })
  })

  return Promise.all(promises)
}

async function loadMusic() {
  const promises = Object.keys(music).map(song => {
    return new Promise((resolve, rejusect) => {
      music[song].src = `../assets/music/${song}.mp3`
      music[song].addEventListener('canplaythrough', resolve)
    })
  })

  return Promise.all(promises)
}

async function loadSFX() {
  const promises = Object.keys(sfx).map(sound => {
    return new Promise((resolve, rejusect) => {
      sfx[sound].src = `../assets/sfx/${sound}.wav`
      sfx[sound].addEventListener('canplaythrough', resolve)
    })
  })

  return Promise.all(promises)
}

async function loadSprites() {
  const promises = Object.keys(sprites).map(sprite => {
    return new Promise((resolve, reject) => {
      sprites[sprite].src = `../assets/sprites/${sprite}.png`
      sprites[sprite].addEventListener('load', resolve)
    })
  })

  return Promise.all(promises)
}

export { loadAssets }
