const maps = {
  links_house_layer1: new Image(),
  links_house_layer2: new Image(),
  //
  ext_0_0_layer1: new Image(),
  ext_0_0_layer2: new Image(),
}

const music = {
  kakariko_village: new Audio(),
  overworld: new Audio(),
}

const sfx = {
  boomerang: new Audio(),
  break: new Audio(),
  bush: new Audio(),
  chest_open: new Audio(),
  cursor: new Audio(),
  heart: new Audio(),
  error: new Audio(),
  lift: new Audio(),
  menu_close: new Audio(),
  menu_open: new Audio(),
  message: new Audio(),
  message_finish: new Audio(),
  purse: new Audio(),
  purse_full: new Audio(),
  rupee: new Audio(),
  sword: new Audio(),
  sword_charged: new Audio(),
  sword_spin: new Audio(),
  sword_shine: new Audio(),
  throw: new Audio(),
  tink: new Audio(),
  treasure: new Audio(),
}

const sprites = {
  destructibles: new Image(),
  dialogue: new Image(),
  hud: new Image(),
  items: new Image(),
  link_green: new Image(),
  loot: new Image(),
  menu: new Image(),
  objects: new Image(),
  shadows: new Image(),
  shield: new Image(),
  sword: new Image(),
  vfx: new Image(),
}

export { maps, music, sfx, sprites }
