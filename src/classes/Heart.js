import { ctx, debugMode } from '../screen.js'
import { player } from '../player.js'
import { sprites } from '../assets.js'
import { game } from '../game.js'
import { playSound } from '../audio.js'
import { menu } from '../menu.js'

class Heart {
  constructor(x = 0, y = 0, fall = false) {
    this.pos = { x, y }
    this.hitbox = { pos: { x: 0, y: 0 }, width: 8, height: 7 }
    this.fall = fall
    this.visible = true
    this.flashInterval = null
    this.time = 0
    this.timeInterval = null
  }
  clear() {
    if (this.flashInterval != null) {
      clearInterval(this.flashInterval)
      clearInterval(this.timeInterval)
      this.flashInterval = null
      this.timeInterval = null
    }
    if (game.map.loot.indexOf(this) != -1) game.map.loot.splice(game.map.loot.indexOf(this), 1)
  }
  collide() {
    playSound('heart')
    player.hp += 4
    if (player.hp > player.hpMax) player.hp = player.hpMax
    this.clear()
  }
  draw() {
    // TODO: fall animation on monster loot
    if (this.visible) {
      ctx.drawImage(
        sprites.shadows,
        20,
        0,
        6,
        4,
        Math.round(this.pos.x + game.map.offset.x + 1),
        Math.round(this.pos.y + game.map.offset.y + 5),
        6,
        4
      )
      ctx.drawImage(
        sprites.loot,
        0,
        7,
        8,
        7,
        Math.round(this.pos.x + game.map.offset.x),
        Math.round(this.pos.y + game.map.offset.y),
        8,
        7
      )
    }
    //
    if (debugMode()) {
      ctx.fillStyle = 'rgba(0, 0, 255, 0.5)'
      ctx.fillRect(
        Math.round(this.pos.x + this.hitbox.pos.x + game.map.offset.x),
        Math.round(this.pos.y + this.hitbox.pos.y + game.map.offset.y),
        this.hitbox.width,
        this.hitbox.height
      )
    }
  }
  flash() {
    this.flashInterval = setInterval(() => {
      if (this.visible) this.visible = false
      else this.visible = true
    }, 1000 / 16)
    setTimeout(() => {
      this.clear()
    }, 2000)
  }
  init() {
    setInterval(() => {
      if (!menu.opened) {
        this.time++
        if (this.time >= 8) this.clear()
        if (this.time >= 6 && this.flashInterval != null) this.flash()
      }
    }, 1000)
  }
}

export { Heart }
