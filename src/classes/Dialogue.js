import { ctx, screen, getScale } from '../screen.js'
import { game } from '../game.js'
import { sprites } from '../assets.js'
import { playSound } from '../audio.js'
import { player } from '../player.js'
import { menu } from '../menu.js'

class Dialogue {
  constructor(interlocutor = null, align = 'left') {
    this.opacity = 0
    this.slide = 0
    this.slides = []
    this.cursor = {
      pos: null,
      speed: 0,
      interval: null,
    }
    this.interlocutor = interlocutor
    this.canAdvance = false
    this.clearing = false
    this.interval = null
    this.drawnLines = []
    this.line = 0
    this.char = 0
    this.align = align
    this.chest = null
  }
  addChar() {
    if (this.char % 4 == 0) playSound('message')
    let letter = this.slides[this.slide][this.line].text.charAt(this.char)
    if (this.drawnLines[this.line] == undefined) this.drawnLines[this.line] = ''
    this.drawnLines[this.line] += letter
    this.char++
    if (this.char >= this.slides[this.slide][this.line].text.length) {
      this.char = 0
      this.line++
    }
    if (this.line >= this.slides[this.slide].length) {
      clearInterval(this.interval)
      this.interval = null
      this.line = 0
      this.char = 0
      playSound('message_finish')
      this.cursor.speed = 1
      this.cursor.pos = 0
      this.interval = setInterval(() => {
        this.moveCursor()
      }, 1000 / 8)
      this.canAdvance = true
    }
  }
  clear() {
    this.clearing = true
    player.setAction('idle')
    if (this.chest != null) this.chest.drawLoot = false
    clearInterval(this.interval)
    this.interval = setInterval(() => {
      this.fadeOut()
    }, 50)
    this.canAdvance = false
    if (!menu.opened) game.freeze = false
  }
  draw() {
    if (this.opacity > 0) {
      ctx.globalAlpha = this.opacity / 1.8
      ctx.drawImage(
        sprites.dialogue,
        0,
        0,
        200,
        60,
        screen.width / getScale() / 2 - 100,
        screen.height / getScale() - 45,
        200,
        40
      )
      //
      if (this.interlocutor != null) {
        ctx.globalAlpha = this.opacity
        ctx.fillStyle = 'white'
        ctx.textAlign = 'left'
        ctx.textBaseline = 'top'
        ctx.font = '5pt Ubuntu'
        ctx.fillText(this.interlocutor, screen.width / getScale() / 2 - 80, screen.height / getScale() - 49)
      }
      //
      ctx.globalAlpha = 1
      if (this.slide >= this.slides.length && !this.clearing) this.clear()
      else if (!this.clearing) {
        for (let i = 0; i < this.drawnLines.length; i++) {
          let dx = screen.width / getScale() / 2
          let dy
          if (this.align == 'left') dx -= 80
          if (this.slides[this.slide].length == 3) dy = screen.height / getScale() - 41 + i * 12
          if (this.slides[this.slide].length == 2) dy = screen.height / getScale() - 35 + i * 12
          if (this.slides[this.slide].length == 1) dy = screen.height / getScale() - 29
          ctx.textAlign = this.align
          ctx.textBaseline = 'top'
          ctx.font = '6pt Ubuntu'
          if (this.slides[this.slide][i].style != null) ctx.fillStyle = this.slides[this.slide][i].style
          else ctx.fillStyle = 'white'
          ctx.fillText(this.drawnLines[i], dx, dy, 180)
        }
      }
      if (this.canAdvance)
        ctx.drawImage(
          sprites.dialogue,
          0,
          60,
          8,
          6,
          screen.width / getScale() / 2 - 4,
          screen.height / getScale() - 9 + this.cursor.pos,
          8,
          6
        )
      else if (this.interval == null) {
        this.drawnLines = []
        this.interval = setInterval(() => {
          this.addChar()
        }, 25)
      }
    }
  }
  fadeIn() {
    if (this.opacity >= 1) {
      clearInterval(this.interval)
      this.interval = null
      this.opacity = 1
    } else this.opacity += 0.2
  }
  fadeOut() {
    if (this.opacity <= 0) {
      clearInterval(this.interval)
      this.interval = null
      this.slide = 0
      this.drawnLines = []
      game.dialogue = null
      if (this.chest != null) {
        this.chest.drawLoot = false
      }
    } else this.opacity -= 0.2
  }
  init(chest = null) {
    this.clearing = false
    this.chest = chest
    this.opacity = 0
    this.slide = 0
    this.cursor.pos = 0
    game.dialogue = this
    game.freeze = true
    this.interval = setInterval(() => {
      this.fadeIn()
    }, 50)
  }
  moveCursor() {
    let tr = 1
    this.cursor.pos += this.cursor.speed

    if (this.cursor.pos > tr || this.cursor.pos < -tr) this.cursor.speed *= -1
  }
}

export { Dialogue }
