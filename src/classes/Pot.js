import { ctx, debugMode } from '../screen.js'
import { game } from '../game.js'
import { sprites } from '../assets.js'
import { player } from '../player.js'
import { playSound } from '../audio.js'
import { getVFX } from '../classes/VFX.js'

class Pot {
  constructor(x = 0, y = 0, loot = null) {
    this.pos = { x, y }
    this.hitbox = { pos: { x: 0, y: 0 }, width: 16, height: 16, noglide: true }
    this.shadow = {
      draw: false,
      pos: {
        x: 2,
        y: 0,
      },
    }
    this.index = 0
    this.source = {
      x: 0,
      y: 0,
    }
    this.movement = {
      interval: null,
      direction: null,
    }
    this.text = 'Lift'
    this.loot = loot
    this.slashable = true
  }
  break(noproj = false) {
    // TODO: hurt target
    playSound('break')
    getVFX('pot_breaking', this.pos.x, this.pos.y)
    if (this.loot != null) {
      this.loot.pos.x += this.pos.x
      this.loot.pos.y += this.pos.y
      this.loot.init()
      game.map.loot.push(this.loot)
      this.loot = null
    }
    this.clear()
  }
  clear() {
    let index = game.map.destructibles.indexOf(this)
    if (game.map.destructibles.indexOf(this) != -1) game.map.destructibles.splice(index, 1)
    index = game.map.projectiles.friendly.indexOf(this)
    if (index != -1) game.map.projectiles.friendly.splice(index, 1)
    index = game.map.projectiles.hostile.indexOf(this)
    if (index != -1) game.map.projectiles.hostile.splice(index, 1)
  }
  draw() {
    if (this.shadow.draw)
      ctx.drawImage(
        sprites.shadows,
        0,
        0,
        12,
        6,
        Math.round(this.pos.x + this.shadow.pos.x + game.map.offset.x),
        Math.round(this.pos.y + this.shadow.pos.y + game.map.offset.y),
        12,
        6
      )
    ctx.drawImage(
      sprites.destructibles,
      0,
      0,
      16,
      16,
      Math.round(this.pos.x + game.map.offset.x),
      Math.round(this.pos.y + game.map.offset.y),
      16,
      16
    )
    if (debugMode()) {
      ctx.fillStyle = 'rgba(255, 0, 0, 0.5)'
      ctx.fillRect(
        this.pos.x + this.hitbox.pos.x + game.map.offset.x,
        this.pos.y + this.hitbox.pos.y + game.map.offset.y,
        this.hitbox.width,
        this.hitbox.height
      )
    }
  }
  interact() {
    if (player.action == 'sword') return
    playSound('lift')
    player.setAction('lifting', 8)
    player.carrying = this
    player.interaction = null
    game.map.destructibles.splice(game.map.destructibles.indexOf(this), 1)
    //
    if (this.loot != null) {
      this.loot.pos.x += this.pos.x
      this.loot.pos.y += this.pos.y
      this.loot.init()
      game.map.loot.push(this.loot)
      this.loot = null
    }
    //
    if (player.direction == 'down') {
      this.shadow.draw = true
      this.shadow.pos.y = 9
      this.pos.x = player.pos.x + 8
      this.pos.y = player.pos.y + 27
      setTimeout(() => {
        this.shadow.pos.y = 16
        this.pos.x = player.pos.x + 8
        this.pos.y = player.pos.y + 21
        setTimeout(() => {
          this.pos.x = player.pos.x + 8
          this.pos.y = player.pos.y + 1
        }, 250)
      }, 250)
    }
    //
    if (player.direction == 'up') {
      this.pos.x = player.pos.x + 8
      this.pos.y = player.pos.y + 2
      setTimeout(() => {
        this.pos.x = player.pos.x + 8
        this.pos.y = player.pos.y + 1
      }, 500)
    }
    //
    if (player.direction == 'left') {
      this.shadow.draw = true
      this.shadow.pos.y = 10
      this.pos.x = player.pos.x - 1
      this.pos.y = player.pos.y + 15
      setTimeout(() => {
        this.shadow.pos.y = 14
        this.pos.x = player.pos.x + 0
        this.pos.y = player.pos.y + 11
        setTimeout(() => {
          this.pos.x = player.pos.x + 8
          this.pos.y = player.pos.y + 1
        }, 250)
      }, 250)
    }
    //
    if (player.direction == 'right') {
      this.shadow.draw = true
      this.shadow.pos.y = 10
      this.pos.x = player.pos.x + 18
      this.pos.y = player.pos.y + 15
      setTimeout(() => {
        this.shadow.pos.y = 14
        this.pos.x = player.pos.x + 16
        this.pos.y = player.pos.y + 11
        setTimeout(() => {
          this.pos.x = player.pos.x + 8
          this.pos.y = player.pos.y + 1
        }, 250)
      }, 250)
    }
    //
    setTimeout(() => {
      player.setAction('idle')
      this.shadow.pos.y = 24
      this.shadow.draw = false
    }, 500)
  }
  move() {
    if (this.movement.direction == 'up') {
      this.pos.y -= 5
      this.shadow.pos.y--
    }
    if (this.movement.direction == 'down') {
      this.pos.y += 5
      this.shadow.pos.y--
    }
    if (this.movement.direction == 'left') {
      this.pos.x -= 4
      this.pos.y++
      this.shadow.pos.y--
    }
    if (this.movement.direction == 'right') {
      this.pos.x += 4
      this.pos.y++
      this.shadow.pos.y--
    }
    if (
      this.shadow.pos.y < 9 ||
      this.pos.x + this.hitbox.pos.x <= 0 ||
      this.pos.x + this.hitbox.pos.x + this.hitbox.width >= game.map.width ||
      this.pos.y + this.hitbox.pos.y <= 0 ||
      this.pos.y + this.hitbox.pos.y + this.hitbox.height >= game.map.height // ||
      // collision entre projectile et ennemi
    ) {
      clearInterval(this.movement.interval)
      this.break()
    }
  }
  throw() {
    playSound('throw')
    this.shadow.draw = true
    this.movement.direction = player.direction
    game.map.projectiles.friendly.push(this)
    player.carrying = null
    this.movement.interval = setInterval(() => {
      this.move()
    }, 1000 / 60)
  }
}

export { Pot }
