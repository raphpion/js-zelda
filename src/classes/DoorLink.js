import { game } from '../game.js'
import { player } from '../player.js'
import { transitions } from '../screen.js'

class DoorLink {
  constructor(
    x = 0,
    y = 0,
    width = 0,
    height = 0,
    x2 = null,
    y2 = null,
    dest = null,
    direction = 'down',
    distanceIn = 0,
    distanceOut = 0
  ) {
    this.pos = { x, y }
    this.width = width
    this.height = height
    this.destination = { map: dest, x: x2, y: y2 }
    this.direction = direction
    this.distanceIn = distanceIn
    this.distanceOut = distanceOut
    this.interval = null
  }
  collide() {
    if (player.carrying != null) player.carrying.throw()
    game.freeze = true
    let traveled = 0
    transitions.fadeOut.init(1000)
    for (let projectile of game.map.projectiles.friendly) projectile.clear()
    for (let projectile of game.map.projectiles.hostile) projectile.clear()
    game.map.projectiles.friendly = []
    game.map.projectiles.hostile = []
    if (this.distanceIn != 0)
      this.interval = setInterval(() => {
        if (this.direction == 'up') player.pos.y -= player.speed * 0.5
        if (this.direction == 'down') player.pos.y += player.speed * 0.5
        if (this.direction == 'left') player.pos.x -= player.speed * 0.5
        if (this.direction == 'right') player.pos.x += player.speed * 0.5
        traveled += 0.5
        if (traveled >= this.distanceIn) clearInterval(this.interval)
      }, 1000 / 60)
    //
    setTimeout(() => {
      this.destination.map.init()
      player.pos.x = this.destination.x
      player.pos.y = this.destination.y
      transitions.fadeIn.init(1000)
      traveled = 0
      if (this.distanceOut != 0)
        this.interval = setInterval(() => {
          if (this.direction == 'up') player.pos.y -= player.speed * 0.5
          if (this.direction == 'down') player.pos.y += player.speed * 0.5
          if (this.direction == 'left') player.pos.x -= player.speed * 0.5
          if (this.direction == 'right') player.pos.x += player.speed * 0.5
          traveled += 0.5
          if (traveled >= this.distanceOut) {
            clearInterval(this.interval)
            game.freeze = false
          }
        }, 1000 / 60)
    }, 1500)
  }
}

export { DoorLink }
