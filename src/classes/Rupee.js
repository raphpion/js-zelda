import { ctx, debugMode } from '../screen.js'
import { sprites } from '../assets.js'
import { game } from '../game.js'
import { playSound } from '../audio.js'
import { player } from '../player.js'
import { menu } from '../menu.js'

class Rupee {
  constructor(x = 0, y = 0, color = 'green', despawn = true, drawShadow = true, bounce = true) {
    if (color == 'green') {
      this.value = 1
      this.source = { x: 8, y: 0 }
    }
    if (color == 'blue') {
      this.value = 5
      this.source = { x: 8, y: 14 }
    }
    if (color == 'red') {
      this.value = 20
      this.source = { x: 8, y: 28 }
    }
    if (color == 'purple') {
      this.value = 50
      this.source = { x: 32, y: 0 }
    }
    if (color == 'silver') {
      this.value = 100
      this.source = { x: 32, y: 14 }
    }
    if (color == 'gold') {
      this.value = 300
      this.source = { x: 32, y: 28 }
    }
    //
    this.width = 8
    this.height = 14
    this.index = 0
    this.animation = null
    this.offset = 0
    this.visible = true
    //
    this.pos = { x, y }
    this.hitbox = { pos: { x: 0, y: 0 }, width: 0, height: 0 }
    this.shadow = {
      draw: drawShadow,
      pos: { x: 1, y: 12 },
    }
    this.animation = setInterval(() => {
      this.animate()
    }, 1000 / 4)
    this.spawnBounce = bounce
    this.despawn = despawn
    this.time = 0
    this.timeInterval = null
  }
  animate() {
    this.index++
    if (this.index > 2) this.index = 0
  }
  bounce() {
    this.offset = -20
    let interval = setInterval(() => {
      this.offset += 3
      if (this.offset >= 0) {
        clearInterval(interval)
        interval = setInterval(() => {
          this.offset--
          if (this.offset <= -5) {
            clearInterval(interval)
            interval = setInterval(() => {
              this.offset++
              if (this.offset >= 0) {
                this.offset = 0
                clearInterval(interval)
                this.hitbox = { pos: { x: 0, y: 0 }, width: 8, height: 14 }
              }
            }, 1000 / 30)
          }
        }, 1000 / 30)
      }
    }, 1000 / 30)
  }
  clear() {
    if (this.flashInterval != null) {
      clearInterval(this.flashInterval)
      this.flashInterval = null
    }
    clearInterval(this.timeInterval)
    this.timeInterval = null
    if (game.map.loot.indexOf(this) != -1) game.map.loot.splice(game.map.loot.indexOf(this), 1)
  }
  collide() {
    playSound('rupee')
    player.giveRupees(this.value)
    this.clear()
  }
  draw() {
    if (this.visible) {
      if (this.shadow.draw)
        ctx.drawImage(
          sprites.shadows,
          20,
          0,
          6,
          4,
          Math.round(this.pos.x + this.shadow.pos.x + game.map.offset.x),
          Math.round(this.pos.y + this.shadow.pos.y + game.map.offset.y),
          6,
          4
        )
      ctx.drawImage(
        sprites.loot,
        this.source.x + this.index * this.width,
        this.source.y,
        this.width,
        this.height,
        Math.round(this.pos.x + game.map.offset.x),
        Math.round(this.pos.y + game.map.offset.y + this.offset),
        this.width,
        this.height
      )
    }
    //
    if (debugMode()) {
      ctx.fillStyle = 'rgba(0, 0, 255, 0.5)'
      ctx.fillRect(
        Math.round(this.pos.x + this.hitbox.pos.x + game.map.offset.x),
        Math.round(this.pos.y + this.hitbox.pos.y + game.map.offset.y),
        this.hitbox.width,
        this.hitbox.height
      )
    }
  }
  flash() {
    this.flashInterval = setInterval(() => {
      if (this.visible) this.visible = false
      else this.visible = true
    }, 1000 / 16)
  }
  init() {
    if (this.despawn)
      this.timeInterval = setInterval(() => {
        if (!menu.opened) {
          this.time++
          if (this.time >= 8) this.clear()
          if (this.time >= 6 && this.flashInterval != null) this.flash()
        }
      }, 1000)
    if (this.spawnBounce) this.bounce()
    else this.hitbox = { pos: { x: 0, y: 0 }, width: 8, height: 14 }
  }
}

export { Rupee }
