import { ctx } from '../screen.js'
import { game } from '../game.js'
import { sprites } from '../assets.js'
import { playSound } from '../audio.js'
import { dialogue_chestEmpty } from '../dialogues/chestEmpty.js'
import { player } from '../player.js'

class Chest {
  constructor(x = 0, y = 0, loot = null, dialogue = null) {
    this.pos = { x, y }
    this.hitbox = { pos: { x: 0, y: 0 }, width: 16, height: 16, noglide: true }
    this.open = false
    this.drawLoot = false
    this.source = {
      x: 0,
      y: 0,
    }
    this.loot = loot
    this.dialogue = dialogue
    this.text = 'Loot'
    this.interactionDirection = 'up'
  }
  draw() {
    this.source.x = this.open ? 16 : 0
    ctx.drawImage(
      sprites.objects,
      this.source.x,
      this.source.y,
      16,
      16,
      this.pos.x + game.map.offset.x,
      this.pos.y + game.map.offset.y,
      16,
      16
    )
  }
  interact() {
    playSound('chest_open')
    player.action = 'idle'
    this.open = true
    game.freeze = true
    setTimeout(() => {
      playSound(this.loot == null ? 'error' : 'treasure')
      if (this.loot == null) {
        dialogue_chestEmpty.init()
      } else {
        this.dialogue.init(this)
        this.loot.pos.x = this.pos.x + 8 - this.loot.width / 2
        this.loot.pos.y = this.pos.y + 8 - this.loot.height / 2 - 6
        this.drawLoot = true
        this.loot.acquire()
        let interval = setInterval(() => {
          this.loot.pos.y--
          if (this.loot.pos.y + this.loot.height < this.pos.y) {
            clearInterval(interval)
            player.setAction('looting', 8)
            this.loot.pos.y = player.pos.y - this.loot.height + 9
            this.loot.pos.x = player.pos.x - this.loot.width / 2 + 22
          }
        }, 20)
      }
    }, 400)
  }
}

export { Chest }
