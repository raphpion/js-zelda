import { game } from '../game.js'
import { sprites } from '../assets.js'
import { ctx } from '../screen.js'
import { player } from '../player.js'

const effects = [
  'bush_slash',
  'player_sweat',
  'pot_breaking',
  'sword_boom',
  'sword_ready',
  'sword_shine',
  'sword_spark',
  'sword_trail',
]

class VFX {
  constructor(effect) {
    this.effect = effect
    this.pos = { x: 0, y: 0 }
    this.source = { x: 0, y: 0 }
    this.width = 0
    this.height = 0
    this.index = 0
    this.animation = null
  }
  animate() {
    if (this.effect == 'bush_slash') {
      let indexLimit = 7
      if (this.index > indexLimit) this.clear()
      this.source.x = this.index * 29
      this.source.y = 28
    }
    if (this.effect == 'player_sweat') {
      if (this.index > 1) this.clear()
    }
    if (this.effect == 'pot_breaking') {
      let indexLimit = 7
      if (this.index > indexLimit) this.clear()
      this.source.x = this.index * 30
      this.source.y = 0
    }
    if (this.effect == 'sword_boom') {
      let indexLimit = 3
      if (this.index > indexLimit) this.clear()
      this.source.x = 240
      this.source.y = 0 + this.index * 16
    }
    if (this.effect == 'sword_ready') {
      let indexLimit = 0
      if (this.index > indexLimit) this.clear()
      this.source.x = 232
      this.source.y = 81
    }
    if (this.effect == 'sword_shine') {
      let indexLimit = 2
      if (this.index > indexLimit) this.clear()
      this.source.x = 232
      this.source.y = 60 + this.index * 7
    }
    if (this.effect == 'sword_spark') {
      let indexLimit = 2
      if (this.index > indexLimit) this.clear()
      this.source.x = 256
      this.source.y = 0 + this.index * 14
    }
    if (this.effect == 'sword_trail') {
      let indexLimit = 3
      if (this.index > indexLimit) this.clear()
      this.source.x = 240 + player.swordSprite.trailColor * 11
      this.source.y = 64 + this.index * 11
    }
    this.index++
  }
  clear() {
    clearInterval(this.animation)
    if (game.map.vfx.indexOf(this) != -1) game.map.vfx.splice(game.map.vfx.indexOf(this), 1)
  }
  draw() {
    if (this.effect == 'player_sweat') drawPlayerSweat(this)
    else if (this.effect == 'sword_ready') {
      if (player.swordSprite.direction == 'up') {
        this.pos.x = player.swordSprite.pos.x + player.pos.x + 3
        this.pos.y = player.swordSprite.pos.y + player.pos.y - 6
      }
      if (player.swordSprite.direction == 'down') {
        this.pos.x = player.swordSprite.pos.x + player.pos.x - 4
        this.pos.y = player.swordSprite.pos.y + player.swordSprite.height + player.pos.y - 3
      }
      if (player.swordSprite.direction == 'left') {
        this.pos.x = player.swordSprite.pos.x + player.pos.x - 5
        this.pos.y = player.swordSprite.pos.y + player.pos.y - 2
      }
      if (player.swordSprite.direction == 'right') {
        this.pos.x = player.swordSprite.pos.x + player.swordSprite.width + player.pos.x - 4
        this.pos.y = player.swordSprite.pos.y + player.pos.y - 2
      }
    }
    ctx.drawImage(
      sprites.vfx,
      this.source.x,
      this.source.y,
      this.width,
      this.height,
      Math.round(this.pos.x + game.map.offset.x),
      Math.round(this.pos.y + game.map.offset.y),
      this.width,
      this.height
    )
  }
  init(x, y) {
    let interval
    if (this.effect == 'bush_slash') {
      this.pos.x = x - 8
      this.pos.y = y - 18
      this.width = 29
      this.height = 43
      interval = 8
    }
    if (this.effect == 'player_sweat') {
      interval = 8
    }
    if (this.effect == 'pot_breaking') {
      this.pos.x = x - 8
      this.pos.y = y - 4
      this.width = 30
      this.height = 28
      interval = 8
    }
    if (this.effect == 'sword_boom') {
      this.pos.x = x
      this.pos.y = y
      this.width = 16
      this.height = 16
      interval = 16
    }
    if (this.effect == 'sword_ready') {
      this.width = 8
      this.height = 8
      interval = 8
    }
    if (this.effect == 'sword_shine') {
      this.pos.x = x
      this.pos.y = y
      this.width = 7
      this.height = 7
      interval = 8
    }
    if (this.effect == 'sword_spark') {
      this.pos.x = x
      this.pos.y = y
      this.width = 14
      this.height = 14
      interval = 16
    }
    if (this.effect == 'sword_trail') {
      this.pos.x = x
      this.pos.y = y
      this.width = 11
      this.height = 11
      interval = 24
    }
    //
    if (this.effect != 'player_sweat') this.animate()
    this.animation = setInterval(() => {
      this.animate()
    }, 1000 / interval)
  }
}

function getVFX(effect, x = null, y = null) {
  if (!effects.includes(effect)) return
  let vfx = new VFX(effect)
  vfx.init(x, y)
  game.map.vfx.push(vfx)
}
//
function drawPlayerSweat(vfx) {
  if (player.direction == 'up') {
    if (vfx.index == 0) {
      ctx.drawImage(
        sprites.vfx,
        232,
        28,
        8,
        8,
        Math.round(player.pos.x + 1 + game.map.offset.x),
        Math.round(player.pos.y + 16 + game.map.offset.y),
        8,
        8
      )
      ctx.drawImage(
        sprites.vfx,
        232,
        44,
        8,
        8,
        Math.round(player.pos.x + 23 + game.map.offset.x),
        Math.round(player.pos.y + 16 + game.map.offset.y),
        8,
        8
      )
    }
    if (vfx.index == 1) {
      ctx.drawImage(
        sprites.vfx,
        232,
        36,
        8,
        8,
        Math.round(player.pos.x - 1 + game.map.offset.x),
        Math.round(player.pos.y + 20 + game.map.offset.y),
        8,
        8
      )
      ctx.drawImage(
        sprites.vfx,
        232,
        52,
        8,
        8,
        Math.round(player.pos.x + 25 + game.map.offset.x),
        Math.round(player.pos.y + 20 + game.map.offset.y),
        8,
        8
      )
    }
  }
  if (player.direction == 'down') {
    if (vfx.index == 0) {
      ctx.drawImage(
        sprites.vfx,
        232,
        28,
        8,
        8,
        Math.round(player.pos.x + 1 + game.map.offset.x),
        Math.round(player.pos.y + 16 + game.map.offset.y),
        8,
        8
      )
      ctx.drawImage(
        sprites.vfx,
        232,
        44,
        8,
        8,
        Math.round(player.pos.x + 23 + game.map.offset.x),
        Math.round(player.pos.y + 16 + game.map.offset.y),
        8,
        8
      )
    }
    if (vfx.index == 1) {
      ctx.drawImage(
        sprites.vfx,
        232,
        36,
        8,
        8,
        Math.round(player.pos.x - 1 + game.map.offset.x),
        Math.round(player.pos.y + 20 + game.map.offset.y),
        8,
        8
      )
      ctx.drawImage(
        sprites.vfx,
        232,
        52,
        8,
        8,
        Math.round(player.pos.x + 25 + game.map.offset.x),
        Math.round(player.pos.y + 20 + game.map.offset.y),
        8,
        8
      )
    }
  }
  if (player.direction == 'left') {
    if (vfx.index == 0)
      ctx.drawImage(
        sprites.vfx,
        232,
        44,
        8,
        8,
        Math.round(player.pos.x + 21 + game.map.offset.x),
        Math.round(player.pos.y + 16 + game.map.offset.y),
        8,
        8
      )
    if (vfx.index == 1)
      ctx.drawImage(
        sprites.vfx,
        232,
        52,
        8,
        8,
        Math.round(player.pos.x + 23 + game.map.offset.x),
        Math.round(player.pos.y + 20 + game.map.offset.y),
        8,
        8
      )
  }
  if (player.direction == 'right') {
    if (vfx.index == 0)
      ctx.drawImage(
        sprites.vfx,
        232,
        28,
        8,
        8,
        Math.round(player.pos.x + 3 + game.map.offset.x),
        Math.round(player.pos.y + 16 + game.map.offset.y),
        8,
        8
      )
    if (vfx.index == 1)
      ctx.drawImage(
        sprites.vfx,
        232,
        36,
        8,
        8,
        Math.round(player.pos.x + 1 + game.map.offset.x),
        Math.round(player.pos.y + 20 + game.map.offset.y),
        8,
        8
      )
  }
}

export { getVFX }
