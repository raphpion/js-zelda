import { ctx, screen, debugMode, drawBlocks } from '../screen.js'
import { player } from '../player.js'
import { maps } from '../assets.js'
import { game } from '../game.js'
import { Pot } from '../classes/Pot.js'
import { playMusic } from '../audio.js'
import { Heart } from '../classes/Heart.js'
import { chests } from '../objects/chests.js'
import { DoorLink } from '../classes/DoorLink.js'
import { map_ext_0_0 } from './ext_0_0.js'

const map_links_house = {
  background: '#392829',
  width: 224,
  height: 176,
  offset: {
    x: 0,
    y: 0,
  },
  blocks: [],
  objects: [],
  destructibles: [],
  loot: [],
  projectiles: {
    friendly: [],
    hostile: [],
  },
  vfx: [],
  mapLinks: [],
  draw: () => {
    ctx.drawImage(maps.links_house_layer1, game.map.offset.x, game.map.offset.y, game.map.width, game.map.height)
    for (let obj of game.map.destructibles) obj.draw()
    for (let obj of game.map.objects) obj.draw()
    for (let obj of game.map.objects) if (obj.drawLoot != null && obj.drawLoot) obj.loot.draw()
    for (let obj of game.map.loot) obj.draw()
    player.draw()
    for (let obj of game.map.projectiles.friendly) obj.draw()
    for (let obj of game.map.projectiles.hostile) obj.draw()
    for (let obj of game.map.vfx) obj.draw()
    ctx.drawImage(maps.links_house_layer2, game.map.offset.x, game.map.offset.y, game.map.width, game.map.height)
    ctx.fillStyle = '#392829'
    ctx.fillRect(game.map.offset.x, game.map.offset.y + game.map.height, game.map.width, 32)
    if (debugMode()) drawBlocks(game.map)
  },
  init: () => {
    playMusic('kakariko_village')
    game.map = map_links_house
    screen.style.background = game.map.background
    game.map.blocks = []
    game.map.blocks.push(
      { pos: { x: 0, y: 0 }, width: 24, height: 176, blockProjectiles: true }, // left wall
      { pos: { x: 200, y: 0 }, width: 24, height: 176, blockProjectiles: true }, // right wall
      { pos: { x: 24, y: 152 }, width: 80, height: 56, blockProjectiles: true }, // bottom-left wall
      { pos: { x: 120, y: 152 }, width: 80, height: 56, blockProjectiles: true }, // bottom-right wall
      { pos: { x: 24, y: 0 }, width: 176, height: 24, blockProjectiles: true }, // top wall
      { pos: { x: 144, y: 48 }, width: 16, height: 16 }, // upper bench
      { pos: { x: 144, y: 96 }, width: 16, height: 16 }, // lower bench
      { pos: { x: 128, y: 64 }, width: 48, height: 32 }, // right table
      { pos: { x: 40, y: 112 }, width: 32, height: 24 }, // left table
      { pos: { x: 40, y: 24 }, width: 32, height: 40 } // bed
    )
    game.map.destructibles = []
    for (let i = 0; i < 3; i++) {
      game.map.destructibles.push(new Pot(24, 24 + i * 16, new Heart(4, 1)))
    }
    game.map.objects = []
    game.map.objects.push(chests.deku_shield, chests.kokiri_sword, chests.green_purse)
    game.map.loot = []
    game.map.mapLinks = []
    game.map.mapLinks.push(new DoorLink(104, 176, 16, 16, 312, 320, map_ext_0_0, 'down', 32, 18))
    game.map.vfx = []
  },
}

export { map_links_house }
