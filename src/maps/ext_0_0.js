import { ctx, screen, debugMode, drawBlocks } from '../screen.js'
import { playMusic } from '../audio.js'
import { Bush } from '../classes/Bush.js'
import { Rupee } from '../classes/Rupee.js'
import { game } from '../game.js'
import { maps } from '../assets.js'
import { player } from '../player.js'
import { DoorLink } from '../classes/DoorLink.js'
import { map_links_house } from './links_house.js'
import { chests } from '../objects/chests.js'

const map_ext_0_0 = {
  background: '#489848',
  width: 640,
  height: 640,
  offset: {
    x: 0,
    y: 0,
  },
  blocks: [],
  objects: [],
  destructibles: [],
  loot: [],
  projectiles: {
    friendly: [],
    hostile: [],
  },
  vfx: [],
  mapLinks: [],
  draw: () => {
    ctx.drawImage(maps.ext_0_0_layer1, game.map.offset.x, game.map.offset.y, game.map.width, game.map.height)
    for (let obj of game.map.destructibles) obj.draw()
    for (let obj of game.map.objects) obj.draw()
    for (let obj of game.map.objects) if (obj.drawLoot != null && obj.drawLoot) obj.loot.draw()
    for (let obj of game.map.loot) obj.draw()
    player.draw()
    for (let obj of game.map.projectiles.friendly) obj.draw()
    for (let obj of game.map.projectiles.hostile) obj.draw()
    for (let obj of game.map.vfx) obj.draw()
    ctx.drawImage(maps.ext_0_0_layer2, game.map.offset.x, game.map.offset.y, game.map.width, game.map.height)
    ctx.fillStyle = '#392829'
    if (debugMode()) drawBlocks(game.map)
  },
  init: () => {
    playMusic('overworld')
    game.map = map_ext_0_0
    screen.style.background = game.map.background
    game.map.blocks = []
    game.map.blocks.push(
      { pos: { x: 280, y: 281 }, width: 96, height: 71, blockProjectiles: true }, // Link's house roof
      { pos: { x: 280, y: 352 }, width: 40, height: 16, blockProjectiles: true }, // Link's house front left
      { pos: { x: 336, y: 352 }, width: 40, height: 16, blockProjectiles: true } // Link's house front right
    )
    game.map.destructibles = []
    let bushPos = [
      { x: 248, y: 368 },
      { x: 264, y: 368 },
      { x: 280, y: 368 },
      { x: 360, y: 368 },
      { x: 376, y: 368 },
      { x: 248, y: 384 },
      { x: 264, y: 384 },
      { x: 280, y: 384 },
      { x: 360, y: 384 },
      { x: 376, y: 384 },
    ]
    for (let i = 0; i < bushPos.length; i++) {
      let r = Math.ceil(Math.random() * 100 + 1)
      let loot = null
      if (r >= 95) loot = new Rupee(4, 0, 'blue')
      else if (r >= 75) loot = new Rupee(4, 0, 'green')
      game.map.destructibles.push(new Bush(bushPos[i].x, bushPos[i].y, loot))
    }
    game.map.loot = []
    game.map.objects = []
    game.map.objects.push(chests.boomerang)
    game.map.mapLinks = []
    game.map.mapLinks.push(new DoorLink(320, 336, 16, 16, 96, 184, map_links_house, 'up', 14, 33))
    game.map.vfx = []
  },
}

export { map_ext_0_0 }
