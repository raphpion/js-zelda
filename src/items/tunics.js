import { ctx } from '../screen.js'
import { sprites } from '../assets.js'
import { game } from '../game.js'

const tunics = {
  green: {
    text: 'Kokiri Tunic',
    pos: { x: 0, y: 0 },
    width: 18,
    height: 13,
    owned: true,
    equipped: true,
    acquire: () => {
      tunics.green.owned = true
    },
    draw: (x = null, y = null) => {
      if (x == null && y == null)
        ctx.drawImage(
          sprites.loot,
          0,
          57,
          tunics.green.width,
          tunics.green.height,
          Math.round(tunics.green.pos.x + game.map.offset.x),
          Math.round(tunics.green.pos.y + game.map.offset.y),
          tunics.green.width,
          tunics.green.height
        )
      else
        ctx.drawImage(
          sprites.loot,
          0,
          57,
          tunics.green.width,
          tunics.green.height,
          x + 3,
          y + 5,
          tunics.green.width,
          tunics.green.height
        )
      if (tunics.green.equipped) ctx.drawImage(sprites.menu, 179, 92, 23, 23, x, y, 23, 23)
    },
    equip: () => {},
  },
  blue: {
    text: 'Zora Tunic',
    pos: { x: 0, y: 0 },
    width: 18,
    height: 15,
    owned: false,
    equipped: false,
    acquire: () => {
      tunics.blue.owned = true
    },
    draw: (x = null, y = null) => {
      if (x == null && y == null)
        ctx.drawImage(
          sprites.loot,
          17,
          57,
          tunics.blue.width,
          tunics.blue.height,
          Math.round(tunics.blue.pos.x + game.map.offset.x),
          Math.round(tunics.blue.pos.y + game.map.offset.y),
          tunics.blue.width,
          tunics.blue.height
        )
      else
        ctx.drawImage(
          sprites.loot,
          17,
          57,
          tunics.blue.width,
          tunics.blue.height,
          x + 3,
          y + 5,
          tunics.blue.width,
          tunics.blue.height
        )
      if (tunics.blue.equipped) ctx.drawImage(sprites.menu, 179, 92, 23, 23, x, y, 23, 23)
    },
  },
  red: {
    text: 'Goron Tunic',
    pos: { x: 0, y: 0 },
    width: 18,
    height: 15,
    owned: false,
    equipped: false,
    acquire: () => {
      tunics.red.owned = true
    },
    draw: (x = null, y = null) => {
      if (x == null && y == null)
        ctx.drawImage(
          sprites.loot,
          34,
          57,
          tunics.red.width,
          tunics.red.height,
          Math.round(tunics.red.pos.x + game.map.offset.x),
          Math.round(tunics.red.pos.y + game.map.offset.y),
          tunics.red.width,
          tunics.red.height
        )
      else
        ctx.drawImage(
          sprites.loot,
          34,
          57,
          tunics.red.width,
          tunics.red.height,
          x + 3,
          y + 5,
          tunics.red.width,
          tunics.red.height
        )
      if (tunics.red.equipped) ctx.drawImage(sprites.menu, 179, 92, 23, 23, x, y, 23, 23)
    },
  },
  load: () => {
    for (let tunic in tunics) {
      if (localStorage.hasOwnProperty(`tunic_${tunic}`)) tunics[tunic].owned = true
    }
  },
  save: () => {
    for (let tunic in tunics)
      if (tunics[tunic].owned && !localStorage.hasOwnProperty(`tunic_${tunic}`))
        localStorage.setItem(`tunic_${tunic}`, true)
  },
}

export { tunics }
