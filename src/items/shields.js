import { ctx } from '../screen.js'
import { sprites } from '../assets.js'
import { game } from '../game.js'
import { playSound } from '../audio.js'
import { player } from '../player.js'

const shields = {
  deku: {
    text: 'Deku Shield',
    pos: { x: 0, y: 0 },
    width: 9,
    height: 11,
    owned: false,
    equipped: false,
    acquire: () => {
      shields.deku.owned = true
    },
    draw: (x = null, y = null, noborder = null) => {
      if (x == null && y == null)
        ctx.drawImage(
          sprites.shield,
          0,
          0,
          shields.deku.width,
          shields.deku.height,
          Math.round(shields.deku.pos.x + game.map.offset.x),
          Math.round(shields.deku.pos.y + game.map.offset.y),
          shields.deku.width,
          shields.deku.height
        )
      else ctx.drawImage(sprites.shield, 0, 0, 9, 11, x + 7, y + 6, 9, 11)
      if (shields.deku.equipped && noborder == null) ctx.drawImage(sprites.menu, 179, 92, 23, 23, x, y, 23, 23)
    },
    equip: () => {
      playSound('sword_shine')
      for (let shield in shields) shields[shield].equipped = false
      shields.deku.equipped = true
      player.shield = shields.deku
    },
  },
  load: () => {
    for (let shield in shields) {
      if (localStorage.hasOwnProperty(`shield_${shield}`)) shields[shield].owned = true
    }
  },
  save: () => {
    for (let shield in shields)
      if (shields[shield].owned && !localStorage.hasOwnProperty(`shield_${shield}`))
        localStorage.setItem(`shield_${shield}`, true)
  },
}

export { shields }
