import { boomerang } from './boomerang.js'

const items = {
  boomerang,
  load: () => {
    for (let item in items) {
      if (localStorage.hasOwnProperty(`${item}`)) items[item].owned = true
    }
  },
  save: () => {
    for (let item in items)
      if (items[item].owned && !localStorage.hasOwnProperty(`${item}`)) localStorage.setItem(`${item}`, true)
  },
}

export { items }
