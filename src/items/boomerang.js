import { ctx, debugMode } from '../screen.js'
import { sprites } from '../assets.js'
import { player } from '../player.js'
import { game } from '../game.js'
import { playSound } from '../audio.js'
import { controller } from '../controller.js'

const boomerang = {
  name: 'boomerang',
  text: 'Boomerang',
  pos: { x: 0, y: 0 },
  width: 13,
  height: 13,
  velocity: { x: 0, y: 0 },
  distance: 80,
  speed: 2,
  traveled: null,
  ready: true,
  returning: false,
  thrown: false,
  loot: [],
  owned: false,
  //
  animation: null,
  movement: null,
  index: 0,
  //
  acquire: () => {
    boomerang.owned = true
  },
  animate: () => {
    if (game.freeze) return
    playSound('boomerang')
    let indexLimit = 3
    boomerang.index++
    if (boomerang.index > indexLimit) boomerang.index = 0
  },
  clear: () => {
    boomerang.ready = true
    boomerang.returning = false
    boomerang.thrown = false
    boomerang.traveled = 0
    clearInterval(boomerang.movement)
    clearInterval(boomerang.animation)
    let i = game.map.projectiles.friendly.indexOf(boomerang)
    if (i != -1) game.map.projectiles.friendly.splice(i, 1)
  },
  draw: (x = null, y = null, noborder = true) => {
    if (x == null && y == null) {
      ctx.drawImage(
        sprites.items,
        boomerang.index * 13,
        0,
        13,
        13,
        Math.round(boomerang.pos.x + game.map.offset.x),
        Math.round(boomerang.pos.y + game.map.offset.y),
        13,
        13
      )
      if (debugMode()) {
        ctx.fillStyle = 'rgba(255, 127, 0, 0.5)'
        ctx.fillRect(
          Math.round(boomerang.pos.x + game.map.offset.x),
          Math.round(boomerang.pos.y + game.map.offset.y),
          boomerang.width,
          boomerang.height
        )
      }
    } else ctx.drawImage(sprites.items, 0, 0, 13, 13, x + 5, y + 5, 13, 13)
    if (player.item1 == boomerang && !noborder) ctx.drawImage(sprites.menu, 179, 115, 23, 23, x, y, 23, 23)
    if (player.item2 == boomerang && !noborder) ctx.drawImage(sprites.menu, 179, 138, 23, 23, x, y, 23, 23)
  },
  move: () => {
    if (game.freeze) return
    if (!boomerang.returning) {
      boomerang.pos.x += boomerang.velocity.x * boomerang.speed
      boomerang.pos.y += boomerang.velocity.y * boomerang.speed
      boomerang.traveled += boomerang.speed
      for (let loot of boomerang.loot) {
        loot.pos.x += boomerang.velocity.x * boomerang.speed
        loot.pos.y += boomerang.velocity.y * boomerang.speed
      }
      if (boomerang.traveled >= boomerang.distance) boomerang.returning = true
    } else {
      if (boomerang.pos.x < player.pos.x + player.hitbox.pos.x) {
        boomerang.pos.x += boomerang.speed
        for (let loot of boomerang.loot) loot.pos.x += boomerang.speed
      }
      if (boomerang.pos.x > player.pos.x + player.hitbox.pos.x) {
        boomerang.pos.x -= boomerang.speed
        for (let loot of boomerang.loot) loot.pos.x -= boomerang.speed
      }
      if (boomerang.pos.y < player.pos.y + player.hitbox.pos.y) {
        boomerang.pos.y += boomerang.speed
        for (let loot of boomerang.loot) loot.pos.y += boomerang.speed
      }
      if (boomerang.pos.y > player.pos.y + player.hitbox.pos.y) {
        boomerang.pos.y -= boomerang.speed
        for (let loot of boomerang.loot) loot.pos.y -= boomerang.speed
      }
    }
  },
  throw: () => {
    playSound('boomerang')
    boomerang.thrown = true
    boomerang.animation = setInterval(boomerang.animate, 1000 / 8)
    if (player.direction == 'down') {
      if (controller.joystick.x != 0) {
        boomerang.velocity.x = controller.joystick.x
        boomerang.velocity.y = controller.joystick.y
      } else {
        boomerang.velocity.x = 0
        boomerang.velocity.y = 1
      }
    }
    if (player.direction == 'up') {
      if (controller.joystick.x != 0) {
        boomerang.velocity.x = controller.joystick.x
        boomerang.velocity.y = controller.joystick.y
      } else {
        boomerang.velocity.x = 0
        boomerang.velocity.y = -1
      }
    }
    if (player.direction == 'left') {
      if (controller.joystick.y != 0) {
        boomerang.velocity.x = controller.joystick.x
        boomerang.velocity.y = controller.joystick.y
      } else {
        boomerang.velocity.x = -1
        boomerang.velocity.y = 0
      }
    }
    if (player.direction == 'right') {
      if (controller.joystick.y != 0) {
        boomerang.velocity.x = controller.joystick.x
        boomerang.velocity.y = controller.joystick.y
      } else {
        boomerang.velocity.x = 1
        boomerang.velocity.y = 0
      }
    }
    boomerang.traveled = 0
    boomerang.movement = setInterval(boomerang.move, 1000 / 60)
  },
  use: () => {
    if (!boomerang.ready) return
    boomerang.ready = false
    player.setAction('boomerangthrow', 16)
    game.map.projectiles.friendly.push(boomerang)
  },
}

export { boomerang }
