import { ctx } from '../screen.js'
import { sprites } from '../assets.js'
import { game } from '../game.js'
import { playSound } from '../audio.js'
import { player } from '../player.js'

const swords = {
  kokiri: {
    text: 'Kokiri Sword',
    pos: { x: 0, y: 0 },
    width: 10,
    height: 15,
    owned: false,
    equipped: false,
    acquire: () => {
      swords.kokiri.owned = true
    },
    draw: (x = null, y = null, noborder = null) => {
      if (x == null && y == null)
        ctx.drawImage(
          sprites.loot,
          0,
          70,
          swords.kokiri.width,
          swords.kokiri.height,
          Math.round(swords.kokiri.pos.x + game.map.offset.x),
          Math.round(swords.kokiri.pos.y + game.map.offset.y),
          swords.kokiri.width,
          swords.kokiri.height
        )
      else ctx.drawImage(sprites.loot, 10, 70, 13, 13, x + 5, y + 5, 13, 13)
      if (swords.kokiri.equipped && noborder == null) ctx.drawImage(sprites.menu, 179, 92, 23, 23, x, y, 23, 23)
    },
    equip: () => {
      playSound('sword_shine')
      for (let sword in swords) swords[sword].equipped = false
      swords.kokiri.equipped = true
      player.sword = swords.kokiri
    },
  },
  load: () => {
    for (let sword in swords) {
      if (localStorage.hasOwnProperty(`sword_${sword}`)) swords[sword].owned = true
    }
  },
  save: () => {
    for (let sword in swords)
      if (swords[sword].owned && !localStorage.hasOwnProperty(`sword_${sword}`))
        localStorage.setItem(`sword_${sword}`, true)
  },
}

export { swords }
