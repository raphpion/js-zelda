import { player } from '../player.js'
import { ctx } from '../screen.js'
import { sprites } from '../assets.js'
import { game } from '../game.js'

const purses = {
  green: {
    text: 'Green Purse',
    pos: { x: 0, y: 0 },
    width: 18,
    height: 15,
    owned: false,
    acquire: () => {
      if (player.purse == null) {
        player.purse = 1
        player.rupees = 0
        player.rupeesMax = 99
      }
      purses.green.owned = true
    },
    draw: (x = null, y = null) => {
      if (x == null && y == null)
        ctx.drawImage(
          sprites.loot,
          0,
          42,
          purses.green.width,
          purses.green.height,
          Math.round(purses.green.pos.x + game.map.offset.x),
          Math.round(purses.green.pos.y + game.map.offset.y),
          purses.green.width,
          purses.green.height
        )
      else
        ctx.drawImage(
          sprites.loot,
          0,
          42,
          purses.green.width,
          purses.green.height,
          x + 3,
          y + 4,
          purses.green.width,
          purses.green.height
        )
    },
  },
  load: () => {
    for (let purse in purses) {
      if (localStorage.hasOwnProperty(`purse_${purse}`)) purses[purse].owned = true
    }
  },
  save: () => {
    for (let purse in purses)
      if (purses[purse].owned && !localStorage.hasOwnProperty(`purse_${purse}`))
        localStorage.setItem(`purse_${purse}`, true)
  },
}

export { purses }
