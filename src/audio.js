import { music, sfx } from './assets.js'

let currentSong

function findAudio(obj, key) {
  let value
  Object.keys(obj).some(function (k) {
    if (k === key) {
      value = obj[k]
      return true
    }
    if (obj[k] && typeof obj[k] === 'object') {
      value = findAudio(obj[k], key)
      return value !== undefined
    }
  })
  return value
}

function playMusic(song, loop = true) {
  if (currentSong != song) {
    let s = findAudio(music, song)
    if (loop) s.loop = true
    else loop = false
    stopMusic()
    //s.volume = 1 / 3
    s.volume = 0
    s.play()
    currentSong = song
  }
}

function playSound(sound) {
  let s = findAudio(sfx, sound)
  s.loop = false
  s.currentTime = 0
  s.play()
}

function stopMusic() {
  for (let song in music) {
    if (!music[song].paused && !music[song].ended) {
      music[song].pause()
      music[song].currentTime = 0
    }
  }
  currentSong = null
}

export { playMusic, playSound, stopMusic }
